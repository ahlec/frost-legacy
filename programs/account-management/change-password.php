<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_POST["current-password"]) || !isset($_POST["new-password"]) || !isset($_POST["confirm-password"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
	
if ($database->querySingle("SELECT count(*) FROM users WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "'") == 0)
	exit ("[Error] The user you are currently logged in under does not exist within the database.");
  
$current_password = $_POST["current-password"];
$new_password = $_POST["new-password"];
$confirm_password = $_POST["confirm-password"];
  
/* Password valid */
if (mb_strlen($new_password) < 6 || mb_strlen($confirm_password) < 6)
  exit ("[Error] The supplied password was not six characters or longer.");
if ($new_password !== $confirm_password)
  exit ("[Error] The new passwords supplied did not match in value. Please confirm your new password.");
if ($database->querySingle("SELECT count(*) FROM users WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND password='" .
	$database->escapeString(md5($current_password)) . "'") == 0)
  exit ("[Error] The password you have supplied as your current password is not your current password.");
  
if ($database->exec("UPDATE users SET password='" . md5($new_password) . "' WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "'") === false)
	exit ("[Error] Could not update the database with the new password.");
exit("success");
?>
