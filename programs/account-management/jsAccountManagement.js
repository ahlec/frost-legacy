function AccountManagement() {}
AccountManagement.changePassword = function()
{
  var changePasswordFrame = new Frame("change-password");
  changePasswordFrame.load("programs/account-management/changePasswordFrame.php");
};
AccountManagement.changeTheme = function()
{
  var themeFrame = new Frame("change-theme");
  themeFrame.load("programs/account-management/changeThemeFrame.php");
};
AccountManagement.passwordChangeResponse = function()
{
  var receptical = document.getElementById('change-password-receptical');

  // Has anything... happened?
  if (receptical.contentWindow.document.body.innerHTML.length == 0)
  {
	return;
  }
  
  if (receptical.contentWindow.document.body.innerHTML == "success")
  {
    Prompt("Your password was changed successfully!", "Password changed", [
                {text: 'Okay', colour:'green', action: function()
		{
			Frame.getByHandle('change-password').close();
		}}]);
	return;
  }
  
  alert (receptical.contentWindow.document.body.innerHTML);
  document.getElementById('change-password-current-password').disabled = false;
  document.getElementById('change-password-new-password').disabled = false;
  document.getElementById('change-password-confirm-password').disabled = false;
  document.getElementById('change-password-button').disabled = false;
  document.getElementById('change-password-close-button').disabled = false;
};

// Add hooks
var menuItem = MenuItem.createButton("Change password", "programs/account-management/changePassword-icon.png");
menuItem.setInteraction(AccountManagement.changePassword);
Taskbar.userMenu.addItem(menuItem, 0);

menuItem = MenuItem.createButton("Change theme", "programs/account-management/changeTheme-icon.png");
menuItem.setInteraction(AccountManagement.changeTheme);
Taskbar.userMenu.addItem(menuItem, 1);

delete menuItem;
