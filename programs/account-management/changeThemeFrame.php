<?php
require_once ("../../framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  width:'600px',\n";
  echo "  height:'500px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'Change theme',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  sidebar:'right',\n";
  echo "  icon:'icon-theme.php',\n";
  echo "  onClose:'vacateChangeTheme()'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<style>
div.changeThemeIcon
{
  border:1px solid black;
  margin:3px;
  width:75px;
  height:75px;
  display:inline-block;
  cursor:pointer;
  background-repeat:no-repeat;
  position:relative;
}
div.changeThemeIcon.viewing
{
  border:2px solid blue;
  margin:2px;
  cursor:default;
}
div.changeThemeIcon.current
{
  border-color:green;
}
div.changeThemeIcon div.identifyingIcon
{
  position:absolute;
  left:0px;
  top:0px;
  width:16px;
  height:16px;
  background-color:white;
  border-bottom-right-radius:3px;
  border-right:1px solid black;
  border-bottom:1px solid black;
  display:none;
  background-repeat:no-repeat;
  background-position:center center;
}
div.changeThemeIcon.viewing div.identifyingIcon
{
  border-width:2px;
  border-color:blue;
  display:block;
  background-image:url('images/icons/preview.png');
}
div.changeThemeIcon.current div.identifyingIcon
{
  border-color:green;
  display:block;
  background-image:url('images/icons/tick.png');
}
div.changeThemeIcon div.name
{
  position:absolute;
  left:0px;
  right:0px;
  top:24px;
  text-align:center;
  background-color:white;
  border-top:1px solid black;
  border-bottom:1px solid black;
  display:none;
  padding:2px;
}
div.changeThemeIcon.hovered div.name { display:block; }
</style>";

echo "<script>
var currentlySampledTheme = null;
var finishedApplyingSample = true;
var confirmingTheme = false;
sampleTheme = function(identity)
{
  if (identity == Theme.currentTheme || !finishedApplyingSample || Theme.applyingTheme ||
	confirmingTheme)
    return;
  finishedApplyingSample = false;
  document.getElementById('changeTheme-icon' + Theme.currentTheme).className =
	document.getElementById('changeTheme-icon' + Theme.currentTheme).className.replace(' viewing',
	'');
  executeAJAX('scripts/get-theme-information.php?theme=' + identity, function parse(results)
  {
    var sidebar = Frame.getByHandle('change-theme').sidebar;
    while (sidebar.hasChildNodes())
      sidebar.removeChild(sidebar.firstChild);
    var themeInformation = eval('(' + results + ')');
    var name = document.createElement('div');
    name.innerHTML = themeInformation.name;
    name.className = 'changeThemeSidebarName';
    sidebar.appendChild(name);
    var icon = document.createElement('div');
    icon.className = 'changeThemeIcon';
    icon.style.backgroundImage = 'url(\"" . URL_ROOT . "/themes/icons/\"' +
	themeInformation.icon + ')';
    sidebar.appendChild(icon);
    var confirm = document.createElement('input');
    confirm.className = 'confirmButton';
    confirm.type = 'button';
    confirm.value = 'Confirm';
    confirm.onclick = function()
    {
	confirmingTheme = true;
	executeAJAX('scripts/change-user.php?aspect=theme&id=' + identity,
	  function confirmSuccess(confirmResults)
	  {
	    confirmingTheme = false;
	    if (confirmResults == 'success')
	    {
	      document.getElementById('changeTheme-icon' + Profile.Theme).className =
		document.getElementById('changeTheme-icon' + Profile.Theme).className.replace(' current',
		'');
	      Profile.Theme = identity;
	      document.getElementById('changeTheme-icon' + identity).className += ' current';
	      return;
	    }
	    alert(confirmResults);
	  });
    };
    sidebar.appendChild(confirm);
    finishedApplyingSample = true;
  });
  Theme.apply(identity);
  currentlySampledTheme = identity;
  document.getElementById('changeTheme-icon' + identity).className += ' viewing';
};
vacateChangeTheme = function()
{
  if (Theme.currentTheme != Profile.Theme)
    Theme.apply(Profile.Theme);
};
</script>\n";

$current_theme = $database->querySingle("SELECT theme FROM users WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' LIMIT 1");

$themes = $database->query("SELECT ID, name, icon_file FROM themes");
while ($theme = $themes->fetchArray())
{
  echo "<div class=\"changeThemeIcon" . ($current_theme == $theme["ID"] ? " current viewing" :
	"") ."\" " .
	"style=\"background:url('" . URL_ROOT . "/themes/icons/" .
	$theme["icon_file"] . "');\" onmouseover=\"this.className += ' hovered';\" " .
	"onmouseout=\"this.className = this.className.replace(' hovered', '');\" " .
	"onclick=\"sampleTheme('" . $theme["ID"] . "');\" id=\"changeTheme-icon" .
	$theme["ID"] . "\" title=\"" . $theme["name"] . "\">\n";
  echo "  <div class=\"identifyingIcon\"></div>\n";
//  echo "  <div class=\"name\">" . $theme["name"] . "</div>\n";
  echo "</div>\n";
}

?>
