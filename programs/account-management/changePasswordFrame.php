<?php
require_once ("../../framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  width:'300px',\n";
  echo "  height:'400px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'Change Password',\n";
  echo "  onTaskbar:'true',\n";
  echo "  modal:'true',\n";
  echo "  icon:'programs/account-management/changePassword-icon.png',\n";
  echo "  icon16:'programs/change-password/changePassword-icon16.png'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<script>
changePassword = function()
{
  if (document.getElementById('change-password-current-password').value.length == 0 ||
      document.getElementById('change-password-new-password').value.length == 0 ||
      document.getElementById('change-password-confirm-password').value.length == 0 ||
      document.getElementById('change-password-new-password').value != document.getElementById('change-password-confirm-password').value)
  {
    return;
  }

  document.getElementById('change-password-form').submit();
  document.getElementById('change-password-current-password').disabled = true;
  document.getElementById('change-password-new-password').disabled = true;
  document.getElementById('change-password-confirm-password').disabled = true;
  document.getElementById('change-password-button').disabled = true;
  document.getElementById('change-password-close-button').disabled = true;
};
validateChangePassword = function()
{
  document.getElementById('change-password-button').disabled = (document.getElementById('change-password-current-password').value.length < 6 ||
      document.getElementById('change-password-new-password').value.length < 6 ||
      document.getElementById('change-password-confirm-password').value.length < 6 ||
      document.getElementById('change-password-new-password').value != document.getElementById('change-password-confirm-password').value);
};
</script>\n";

echo "<iframe name=\"change_password_receptical\" id=\"change-password-receptical\" style=\"display:none;\" onload=\"AccountManagement.passwordChangeResponse();\"></iframe>\n";
echo "<form method=\"post\" action=\"" . WEB_PATH . "/programs/account-management/change-password.php\" target=\"" .
	"change_password_receptical\" id=\"change-password-form\">\n";
echo "<div>Current Password:</div>\n";
echo "<input type=\"password\" id=\"change-password-current-password\" name=\"current-password\"  " .
	"onkeyup=\"validateChangePassword();\" />\n";
echo "<div>New Password:</div>\n";
echo "<input type=\"password\" id=\"change-password-new-password\" name=\"new-password\" " .
	"onkeyup=\"validateChangePassword();\" />\n";
echo "<input type=\"password\" id=\"change-password-confirm-password\" name=\"confirm-password\" " .
	"onkeyup=\"validateChangePassword();\" />\n";
echo "<input type=\"button\" id=\"change-password-button\" onclick=\"changePassword();\" value=\"Change Password\" disabled=\"disabled\" />\n";
echo "<input type=\"button\" value=\"Cancel\" id=\"change-password-close-button\" " .
	"onclick=\"Frame.getByHandle('change-password').close();\" />\n";
echo "</form>\n";
?>
