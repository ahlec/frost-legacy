<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");
if (!isset($_GET["parameter"]))
  exit ("Parameters were not passed.");

$target_information = $database->querySingle("SELECT global_ids.type, IFNULL(files.name, locations.name) AS \"name\" " .
	"FROM global_ids LEFT JOIN files ON " .
	"files.gID = global_ids.gID AND global_ids.type = 'file' LEFT JOIN locations ON locations.gID = " .
	"global_ids.gID AND global_ids.type = 'location' JOIN users ON users.uID = global_ids.uID " .
	"WHERE global_ids.gID='" . $database->escapeString($_GET["parameter"]) . "' AND uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' LIMIT 1", true);
if ($target_information === false)
  exit("Target item does not exist or does not belong to you.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'300px',\n";
  echo "  height:'400px',\n";
  echo "  title:'" . mb_addslashes($target_information["name"]) . " :: Properties',\n";
  echo "  icon:'programs/program-selector/icon-viewProperties.png',\n";
  echo "  canClose:'true',\n";
  echo "  canMove:'true',\n";
  echo "  onTaskbar:'true'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<b>Filename:</b> " . $target_information["name"] . "<br />\n";
echo "<b>Global ID:</b> " . $_GET["parameter"] . "<br />\n";
echo "<b>Type:</b> " . $target_information["type"] . "<br />\n";
if ($target_information["type"] == "file")
{
  $file_information = $database->querySingle("SELECT mime, size, dateIntroduced, dateLastUpdated FROM files WHERE gID='" .
  	$database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);
  echo "&ndash; <b>MIME:</b> " . $file_information["mime"] . "<br />\n";
  echo "&ndash; <b>Size:</b> " . $file_information["size"] . "b<br />\n";
}
?>
