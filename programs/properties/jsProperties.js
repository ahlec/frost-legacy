function Properties() { };

Properties.view = function(context_info)
{
  var viewPropertiesFrame = new Frame("view-properties-" + context_info.gid);
  viewPropertiesFrame.load("programs/properties/view.php", context_info.gid);
}
Properties.viewContextConditional = function(context_info)
{
  if (context_info.gid == 1 || context_info.gid == 2)
    return false;
  return true;
}