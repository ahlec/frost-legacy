<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/files.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  width:'500px',\n";
  echo "  height:'500px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'S&yuml;e Dictionary',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  sidebar:'right',\n";
  echo "  icon:'/programs/saie-dictionary/icon-dictionary.php',\n";
  echo "  hasTabs:'true'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}
echo "<b>Search:</b> <input type=\"text\" id=\"saie-dictionary-search\" /> <input type=\"button\" value=\"Search\" " .
	"onclick=\"executeAJAX('programs/saie-dictionary/search.php?query=' + " .
	"document.getElementById('saie-dictionary-search').value, function(results) {" .
	"document.getElementById('saie-dictionary-results').innerHTML = results; });\" />\n";
echo "<div id=\"saie-dictionary-results\"></div>\n";

?>
