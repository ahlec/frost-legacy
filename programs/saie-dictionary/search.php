<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/files.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
require_once ("config.php");

@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

$database = new VersatileDatabase(DICTIONARY_HOST, DICTIONARY_USER, DICTIONARY_PASSWORD, DICTIONARY_DATABASE);

$search_term = $database->escapeString($_GET["query"]);

$results = $database->query("SELECT entry_id, tifon FROM entries WHERE tifon LIKE '%" . $search_term . "%'");
while ($result = $results->fetchArray())
  echo "<div onclick=\"Frame.getByHandle('saie-dictionary').openTab('" . $result["entry_id"] . "','" . 
	addslashes($result["tifon"]) . "'," .
	"'programs/saie-dictionary/entry.php?identity=" . $result["entry_id"] . "');\" style=\"color:blue;cursor:pointer;\">" .
	$result["tifon"] . "</div>\n";

?>
