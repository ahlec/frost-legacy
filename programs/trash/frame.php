<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You are not logged in.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'absolute',\n";
  echo "  top:'10px',\n";
  echo "  left:'10px',\n";
  echo "  width:'550px',\n";
  echo "  height:'500px',\n";
  echo "  sidebar:'left',\n";
  echo "  canClose:'true',\n";
  echo "  title:'Trash',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  iconSmall:'programs/trash/icon16-full.png'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "trash";

?>