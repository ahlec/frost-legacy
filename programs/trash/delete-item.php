<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_GET["gid"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if ($database->querySingle("SELECT count(*) FROM global_ids LEFT JOIN users ON global_ids.uID = users.uID " .
	"LEFT JOIN files ON (files.gID = global_ids.gID AND global_ids.type = " .
	"'file') LEFT JOIN locations ON (locations.gID = global_ids.gID AND global_ids.type = 'location') " .
	"WHERE global_ids.gID='" . $database->escapeString($_GET["gid"]) . "' AND uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND IFNULL(files.location, locations.parent_location) = '2'") == 0)
  exit ("The target no longer exists, is not owned by you, or is not currently in the trash bin.");

$targetType = $database->querySingle("SELECT type FROM global_ids WHERE gID='" . $database->escapeString($_GET["gid"]) .
	"' LIMIT 1");

$success = false;
switch ($targetType)
{
  case "location": $success = $database->exec("UPDATE locations SET parent_location = previousLocation, previousLocation='2' " .
	"WHERE gID='" . $database->escapeString($_GET["gid"]) . "'"); break;
  case "file":
  {
    $hashName = $database->querySingle("SELECT hashName FROM files WHERE gID='" . $database->escapeString($_GET["gid"]) . "' LIMIT 1");
    if (file_exists(IMPEC_PATH . "/files/" . $hashName) && is_file(IMPEC_PATH . "/files/" . $hashName))
      if (!unlink(IMPEC_PATH . "/files/" . $hashName))
        exit("[Error] Unable to delete the actual file on the filesystem. Deletion aborted.");
    $success = $database->exec("DELETE FROM files WHERE gID='" . $database->escapeString($_GET["gid"]) . "'");
    $database->exec("DELETE FROM global_ids WHERE gID='" . $database->escapeString($_GET["gid"]) . "'");
    if (file_exists(IMPEC_PATH . "/file-icons/" . $hashName) && is_file(IMPEC_PATH . "/file-icons/" . $hashName))
      @unlink(IMPEC_PATH . "/file-icons/" . $hashName);
    break;
  }
}

if (!$success)
  exit ("[Error] There was an error encountered when modifying the database records.");
exit("success");
?>
