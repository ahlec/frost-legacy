function Trash() { }
Trash.contentsCount = 0;
Trash.loaded = false;
Trash.contextEmptyConditional = function()
{
  return (Trash.contentsCount > 0);
}
Trash.load = function()
{
  executeAJAX("programs/trash/get-trash.php", function(results)
  {
    if (results == "")
    {
      alert("there was an error with loading the trash information");
      return;
    }
    var information = eval("(" + results + ")");
    Trash.contentsCount = information.count;
    Trash.loaded = true;
    Trash.updateIcons();
  });
}
Trash.updateIcons = function()
{
  if (!Trash.loaded)
    return;
  var icons = IconItem.getItemsByGId(2);
  for (var index = 0; index < icons.length; index++)
    icons[index].setIcon((Trash.contentsCount == 0 ? "trash-empty.png" : "trash-full.png"));
}
Trash.setContentsCount = function(new_value)
{
  if (!Trash.loaded)
    return;
  Trash.contentsCount = new_value;
  Trash.updateIcons();
}
Trash.open = function()
{
  if (Frame.getByHandle("trash"))
  {
    Frame.getByHandle("trash").focus();
    return;
  }
  var trashFrame = new Frame("trash");
  executeAJAX("scripts/get-location.php?identity=2", function(locationInfo)
  {
    locationInfo = eval("(" + locationInfo + ")");
    trashFrame.getFrameContent().contextHandles = locationInfo.contextHandles;
    trashFrame.getFrameContent().base = { };
    trashFrame.getFrameContent().base.getContextInfo = function() { };
    trashFrame.load("programs/trash/frame.php", undefined, function()
    {
      var contents = new LocationContents(2, locationInfo.contents);
      contents.onselection = function(item)
      {
        trashFrame.loadSidebar("programs/trash/sidebar.php?gid=" + item.getGId());
      };
      contents.onblur = function() { trashFrame.loadSidebar("programs/trash/sidebar.php"); };
      contents.applyTo(trashFrame.getFrameContent());
      Trash.trashLocationContents = contents;
      trashFrame.focus();
    }, false);
  });
}
Trash.trashLocationContents = null;
Trash.reloadFrame = function()
{
  if (!Frame.getByHandle("trash"))
    return;
  executeAJAX("scripts/get-location.php?identity=2", function(locationInfo)
  {
    locationInfo = eval("(" + locationInfo + ")");
    if (Trash.trashLocationContents)
      Trash.trashLocationContents.destroy();
    var contents = new LocationContents(locationInfo.contents);
    contents.applyTo(Frame.getByHandle("trash").getFrameContent());
    Frame.getByHandle("trash").focus();
    Trash.trashLocationContents = contents;
  });
}
Trash.restoreItem = function(context_info)
{
  Prompt("Are you sure you wish to restore this item from the trash?", "Restore from trash?", [
                {text: 'Yes', colour:'green', action: function()
		{
		  executeAJAX("programs/trash/restore-item.php?gid=" + context_info.gid, function(results)
		  {
		    if (results == "success")
		    {
			var iconItems = IconItem.getItemsByGId(context_info.gid);
			for (var index = 0; index < iconItems.length; index++)
			  iconItems[index].destroy();
		        executeAJAX("scripts/get-item.php?gid=" + context_info.gid, function(item)
			{
			  var item = eval("(" + item + ")");
			  var destinations = LocationContents.getByGId(item.location);
			  for (var index = 0; index < destinations.length; index++)
			    destinations[index].addItem(item);
			});
			Trash.setContentsCount(Trash.contentsCount - 1);
			return;
		    }
		    alert(results);
		  });
		}},
                {text: 'No', colour:'red', action: function() {} }
        ]);
}
Trash.deleteItem = function(context_info)
{
  Prompt("Are you sure you wish to permanently delete this item (and all items kept within)?", "Delete?", [
                {text: 'Yes', colour:'green', action: function()
		{
		  executeAJAX("programs/trash/delete-item.php?gid=" + context_info.gid, function(results)
		  {
		    if (results == "success")
		    {
			var iconItems = IconItem.getItemsByGId(context_info.gid);
			for (var index = 0; index < iconItems.length; index++)
			  iconItems[index].destroy();
			Trash.setContentsCount(Trash.contentsCount - 1);
			return;
		    }
		    alert(results);
		  });
		}},
                {text: 'No', colour:'red', action: function() {} }
        ]);
}
Trash.empty = function()
{
  Prompt("Are you sure you wish to permanently delete all items in the trash bin?", "Empty trash?", [
		{text: 'Yes', colour:'green', action: function()
		{
		  executeAJAX("programs/trash/empty-trash.php", function(results)
		  {
			if (results == "success")
			{
			  Trash.setContentsCount(0);
			  return;
			}
			alert(results);
		  });
		}},
		{text: 'No', colour:'red', action: function() {} }
	]);
}

// When the file loads
Trash.load();
