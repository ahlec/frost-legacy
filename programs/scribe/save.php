<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_POST["gID"]) && !isset($_POST["file_contents"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if ($database->querySingle("SELECT count(*) FROM global_ids LEFT JOIN users ON global_ids.uID = users.uID " .
	"WHERE gID='" . $database->escapeString($_POST["gID"]) . "' AND uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "'") == 0)
  exit ("The target no longer exists or is not owned by you.");

$fileInfo = $database->querySingle("SELECT mime, hashName FROM files WHERE gID='" . $database->escapeString($_POST["gID"]) . "' LIMIT 1", true);
if (false === file_put_contents(IMPEC_PATH . "/files/" . $fileInfo["hashName"],
	stripslashes($_POST["file_contents"])))
  exit ("[Error] Could not write to the filesystem.");
require_once (IMPEC_PATH . "/icon-generation-scripts/create-icon.php");
createFileIcon($fileInfo["hashName"], $fileInfo["mime"]);
$database->exec("UPDATE files SET dateLastUpdated='" . date("Y-m-d G:i:s") . "', size='" .
	filesize(IMPEC_PATH . "/files/" . $fileInfo["hashName"]) .
	"' WHERE gID='" . $database->escapeString($_POST["gID"]) . "'");
exit("success");
?>
