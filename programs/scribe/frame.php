<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$file_information = $database->querySingle("SELECT gID, location, mime, hashName, name, size FROM files JOIN " .
	"users ON users.uID = files.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' AND gID='" . $database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  width:'600px',\n";
  echo "  height:'500px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'" . mb_addslashes($file_information["name"]) . "',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  icon16:'programs/scribe/icon16.png',\n";
  echo "  icon:'programs/scribe/icon.png',\n";
  echo "  toolbar: {
	'File': [
		{text:'New', icon:'images/icons/new-folder.png', action:'Scribe.new()'},
		{text:'Open', icon:'images/icons/new-folder.png', action:'Scribe.open()'},
		{text:'Save', icon:'images/icons/new-folder.png', action:'Scribe.save()'},
		{text:'Save as', icon:'images/icons/new-folder.png', action:'Scribe.saveAs()'},
		{text:'Close', icon:'images/icons/new-folder.png', action:'Scribe.close()'}],
	'Help': [
		{text:'Help', icon:'images/icons/new-folder.png', action:'Scribe.showHelp()'}]
	}\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<iframe name=\"submit_frame_" . $file_information["hashName"] . "\" style=\"display:none;\" onload=\"Scribe.saveCompleted(" . $_GET["parameter"] . ");\"></iframe>\n";
echo "<form method=\"POST\" target=\"submit_frame_" . $file_information["hashName"] . "\" action=\"" . WEB_PATH .
	"/programs/scribe/save.php\">\n";
echo "  <input type=\"hidden\" name=\"gID\" value=\"" . $file_information["gID"] . "\"/>\n";
echo "  <textarea style=\"width:100%;height:400px;\" name=\"file_contents\">" .
	file_get_contents(IMPEC_PATH . "/files/" . $file_information["hashName"]) . "</textarea>\n";
echo "  <input type=\"button\" onclick=\"Scribe.save(" . $_GET["parameter"] . ");\" value=\"Save\" />\n";
echo "</form>\n";
?>
