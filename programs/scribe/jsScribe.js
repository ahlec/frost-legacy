function Scribe()
{
}
Scribe.open = function(context_info)
{
  if (Frame.getByHandle("scribe-" + context_info.gid))
  {
    Frame.getByHandle("scribe-" + context_info.gid).focus();
    return;
  }
  var window = new Frame("scribe-" + context_info.gid);
  window.load("programs/scribe/frame.php", context_info.gid, function loaded()
  {
  });
};
Scribe.save = function(gID)
{
  var scribeFrame = Frame.getByHandle("scribe-" + gID);
  if (!scribeFrame)
  {
    return;
  }
  scribeFrame.getFrameContent().getElementsByTagName("form")[0].submit();
  Scribe.beginSave(gID);
};
Scribe.saveCompleted = function(gID)
{
  var scribeFrame = Frame.getByHandle("scribe-" + gID);
  if (!scribeFrame)
  {
    return;
  }
  
  // Has anything... happened?
  var iframe = scribeFrame.getFrameContent().getElementsByTagName("iframe")[0];
  if (iframe.contentWindow.document.body.innerHTML.length == 0)
  {
	return;
  }
  
  if (iframe.contentWindow.document.body.innerHTML != "success")
  {
	alert (iframe.contentWindow.document.body.innerHTML);
  }
  
  var formElements = scribeFrame.getFrameContent().getElementsByTagName("form")[0].childNodes;
  for (var index = 0; index < formElements.length; index++)
  {
    formElements[index].disabled = false;
  }
  
  var icons = IconItem.getItemsByGId(gID);
  for (var index = 0; index < icons.length; index++)
  {
    icons[index].setIcon("get.php?gid=" + gID);
  }
};
Scribe.beginSave = function(gID)
{
  var scribeFrame = Frame.getByHandle("scribe-" + gID);
  if (!scribeFrame)
  {
    return;
  }
  
  var formElements = scribeFrame.getFrameContent().getElementsByTagName("form")[0].childNodes;
  for (var index = 0; index < formElements.length; index++)
  {
    formElements[index].disabled = true;
  }
};
