<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");
if (!isset($_GET["parameter"]))
  exit ("Parameter was not wholly passed.");
  
$target_information = $database->querySingle("SELECT global_ids.type, locations.name " .
	"FROM global_ids JOIN locations ON locations.gID = " .
	"global_ids.gID AND global_ids.type = 'location' LEFT JOIN users ON users.uID = global_ids.uID " .
	"WHERE global_ids.gID='" . $database->escapeString($_GET["parameter"]) . "' AND (uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' OR global_ids.uID = '0') LIMIT 1", true);
if ($target_information === false)
  exit("Target item does not exist or does not belong to you.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'400px',\n";
  echo "  height:'80px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'Create new folder',\n";
  echo "  icon:'programs/basic-file-operations/folder-new.png',\n";
  echo "  modal:'true'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<style>
img.renameItemIcon
{
  float:left;
  width:48px;
  height:48px;
  margin:5px;
}
div.renameBox
{
  margin-top:5px;
  margin-bottom:5px;
  margin-left:60px;
  width:330px;
  text-align:center;
}
div.renameBox input.renameTextbox
{
  width:330px;
  display:block;
}
div.renameBox input.renameButton
{
  width:150px;
  margin-top:2px;
}
</style>";

echo "<script>
  checkNewNameValid = function(value)
  {
    if (value.indexOf(\"%\") != -1 || value.length == 0)
    {
      document.getElementById(\"create-folder-button\").disabled = true;
      document.getElementById(\"new-folder-name\").className = \"renameTextbox invalid\";
      return;
    }
    document.getElementById(\"create-folder-button\").disabled = false;
    document.getElementById(\"new-folder-name\").className = \"renameTextbox valid\";
  };
</script>";

echo "<img src=\"" . WEB_PATH . "/images/file-icons/folder.png\" class=\"renameItemIcon\" />\n";
echo "<div class=\"renameBox\">\n";
echo "<input type=\"text\" id=\"new-folder-name\" value=\"\" " .
	"onKeyUp=\"checkNewNameValid(this.value);\" class=\"renameTextbox invalid\" />\n";
echo "  <input type=\"button\" value=\"Create\" id=\"create-folder-button\" onClick=\"createFolder('" . $_GET["parameter"] . "'," .
	"document.getElementById('new-folder-name').value);\" class=\"renameButton\" disabled=\"disabled\" />\n";
echo "  <input type=\"button\" value=\"Cancel\" onClick=\"Frame.getByHandle('create-folder').close();\" id=\"create-folder-cancel\" " .
	"class=\"renameButton\" />\n";
echo "</div>\n";
?>
