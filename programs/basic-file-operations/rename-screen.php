<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");
if (!isset($_GET["parameter"]))
  exit ("Parameter was not wholly passed.");
$target_information = $database->querySingle("SELECT global_ids.type, IFNULL(files.name, locations.name) AS \"name\" " .
	"FROM global_ids LEFT JOIN files ON " .
	"files.gID = global_ids.gID AND global_ids.type = 'file' LEFT JOIN locations ON locations.gID = " .
	"global_ids.gID AND global_ids.type = 'location' JOIN users ON users.uID = global_ids.uID " .
	"WHERE global_ids.gID='" . $database->escapeString($_GET["parameter"]) . "' AND uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' LIMIT 1", true);
if ($target_information === false)
  exit("Target item does not exist or does not belong to you.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'400px',\n";
  echo "  height:'80px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'Rename',\n";
  echo "  icon:'programs/basic-file-operations/document-properies.png',\n";
  echo "  modal:'true'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<style>
img.renameItemIcon
{
  float:left;
  width:48px;
  height:48px;
  margin:5px;
}
div.renameBox
{
  margin-top:5px;
  margin-bottom:5px;
  margin-left:60px;
  width:330px;
  text-align:center;
}
div.renameBox input.renameTextbox
{
  width:330px;
  display:block;
}
div.renameBox input.renameButton
{
  width:150px;
  margin-top:2px;
}
</style>";

if ($target_information["type"] == "file")
{
  $image_information = $database->querySingle("SELECT files.gID, icon, icon_generated FROM files LEFT JOIN filetype_associations " . 
  	"ON files.mime = filetype_associations.mime WHERE files.gID='" . $database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);
  $icon_image = ($image_information["icon_generated"] == 1 ? "get.php?gid=" . $image_information["gID"] : $image_information["icon"]);
}

echo "<script>
  checkNewNameValid = function(value)
  {
    if (value.indexOf(\"%\") != -1 || value.length == 0)
    {
      document.getElementById(\"rename-button\").disabled = true;
      document.getElementById(\"new-rename\").className = \"renameTextbox invalid\";
      return;
    }
    document.getElementById(\"rename-button\").disabled = false;
    document.getElementById(\"new-rename\").className = \"renameTextbox valid\";
  };
</script>";

echo "<img src=\"" . WEB_PATH . "/images/file-icons/" . $icon_image . "\" class=\"renameItemIcon\" />\n";
echo "<div class=\"renameBox\">\n";
echo "<input type=\"text\" id=\"new-rename\" value=\"" .
	preg_replace('/"/','/\\"/', $target_information["name"]). "\" " .
	"onKeyUp=\"checkNewNameValid(this.value);\" class=\"renameTextbox valid\" />\n";
echo "  <input type=\"button\" value=\"Rename\" id=\"rename-button\" onClick=\"rename('" . $_GET["parameter"] . "'," .
	"document.getElementById('new-rename').value);\" class=\"renameButton\" />\n";
echo "  <input type=\"button\" value=\"Cancel\" onClick=\"Frame.getByHandle('rename').close();\" id=\"rename-cancel\" " .
	"class=\"renameButton\" />\n";
echo "</div>\n";
?>
