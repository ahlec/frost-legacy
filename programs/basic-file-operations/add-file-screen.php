<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");
if (!isset($_GET["parameter"]))
  exit ("Parameters were not passed.");

$location_information = $database->querySingle("SELECT gID, handle, name, parent_location, type, custom_icon FROM locations LEFT JOIN " .
	"users ON users.uID = locations.uID WHERE (locations.uID = '0' OR users.uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"') AND locations.gID='" . $database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);
if ($location_information === false)
  exit ("The destination does not exist or is not yours to access.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'400px',\n";
  echo "  height:'150px',\n";
  echo "  title:'Add File',\n";
  echo "  icon:'programs/basic-file-operations/add-file.png',\n";
  echo "  modal:'true',\n";
  echo "  canClose:'true',\n";
  echo "  onTaskbar:'false'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<iframe name=\"upload-file-frame\" onload=\"uploadFileResults(this.contentWindow.document.body.innerHTML);\" " .
	"id=\"upload-file-frame\" style=\"display:none;\"></iframe>\n";
echo "<form action=\"" . URL_ROOT . "/programs/basic-file-operations/upload-file.php?destination=" . $location_information["gID"] .
	"\" target=\"upload-file-frame\" enctype=\"multipart/form-data\" method=\"post\" onsubmit=\"this.disabled=true;\">\n";

echo "<input type=\"file\" name=\"uploaded-file\" />\n";
echo "<input type=\"submit\" value=\"Add file\" id=\"upload-file\" />\n";
echo "<input type=\"button\" value=\"Cancel\" id=\"cancel-upload\" onclick=\"Frame.getByHandle('add-file').close();\" />\n";
echo "</form>\n";

?>
