<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");
if (!isset($_GET["parameter"]))
  exit ("Parameters were not passed.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'500px',\n";
  echo "  height:'350px',\n";
  echo "  title:'Create new item',\n";
  echo "  icon:'programs/program-selector/icon-createNew.png',\n";
  echo "  modal:'true',\n";
  echo "  canClose:'true',\n";
  echo "  onTaskbar:'false'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<input type=\"text\" id=\"new-item-name\" />\n";
$potential_items = $database->query("SELECT association_id, mime, extension, program FROM filetype_associations WHERE canBeCreated='1'");
while ($option = $potential_items->fetchArray())
{
  echo "<input type=\"button\" value=\"" . $option["mime"] . "\" onClick=\"createNewItem('" . $_GET["parameter"] . "','" .
  	$option["mime"] . "'," . "document.getElementById('new-item-name').value);\" style=\"width:100%;\" />\n";
}
?>
