<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/files.php");
@session_start();

if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_GET["name"]) || !isset($_GET["destination"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$destination_information = $database->querySingle("SELECT gID FROM locations LEFT JOIN users ON users.uID = locations.uID WHERE " .
	"(locations.uID = '0' OR uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "') AND locations.gID='" .
	$database->escapeString($_GET["destination"]) . "' LIMIT 1", true);
if ($destination_information === false)
  exit ("[Error] The destination does not exist or is not your property.");

$user_id = $database->querySingle("SELECT uID FROM users WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' LIMIT 1");
	
$name = $database->escapeString(addslashes($_GET["name"]));

$database->exec("INSERT INTO global_ids(uID, type) VALUES('" . $user_id . "','location')");
$global_id = $database->getLastAutoInc();
if ($global_id === false)
  exit ("[Error] Could not register a unique global ID in the database during creation.");

if (!$database->exec("INSERT INTO locations(gID, uID, name, parent_location, type, created) " .
	"VALUES('" . $global_id . "','" . $user_id . "','" . $name . "','" . $destination_information["gID"] . "','" .
	"Folder','" . date("Y-m-d H:i:s") . "')"))
{
  $database->exec("DELETE FROM global_ids WHERE gID='" . $global_id . "'");
  exit ("[Error] Could not create the folder in the database.");
}

exit("[Success] " . $global_id);
?>
