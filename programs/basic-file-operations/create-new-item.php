<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/files.php");
@session_start();

if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_GET["name"]) || !isset($_GET["destination"]) || !isset($_GET["mime"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$destination_information = $database->querySingle("SELECT gID FROM locations LEFT JOIN users ON users.uID = locations.uID WHERE " .
	"(locations.uID = '0' OR uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "') AND locations.gID='" .
	$database->escapeString($_GET["destination"]) . "' LIMIT 1", true);
if ($destination_information === false)
  exit ("[Error] The destination does not exist or is not your property.");

$user_id = $database->querySingle("SELECT uID FROM users WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' LIMIT 1");
	
$name = $database->escapeString(addslashes($_GET["name"]));

$filetype_information = $database->querySingle("SELECT mime, icon, icon_generated, program, gIDType FROM filetype_associations " .
	"WHERE canBeCreated='1' AND mime='" . $database->escapeString($_GET["mime"]) . "' LIMIT 1", true);
if ($filetype_information === false)
  exit ("[Error] The specified mime does not exist or is not able to be created here at this time.");
  

$database->exec("INSERT INTO global_ids(uID, type) VALUES('" . $user_id . "','" . $filetype_information["gIDType"] . "')");
$global_id = $database->getLastAutoInc();
if ($global_id === false)
  exit ("[Error] Could not register a unique global ID in the database during creation.");

if ($filetype_information["gIDType"] == "location")
{
  if (!$database->exec("INSERT INTO locations(gID, uID, name, parent_location, type, created) " .
	"VALUES('" . $global_id . "','" . $user_id . "','" . $name . "','" . $destination_information["gID"] . "','" .
	$filetype_information["mime"] . "','" . date("Y-m-d H:i:s") . "')"))
  {
    $database->exec("DELETE FROM global_ids WHERE gID='" . $global_id . "'");
    exit ("[Error] Could not create the item in the database.");
  }
} else if ($filetype_information["gIDType"] == "file")
{
  do { $file_hash = randomString(); } while ($database->querySingle("SELECT count(*) FROM files WHERE hashName='" .
	$database->escapeString($file_hash) . "'") != 0);
  if (!touch (IMPEC_PATH . "/files/" . $file_hash))
  {
    $database->exec("DELETE FROM global_ids WHERE gID='" . $global_id . "'");
    $database->exec("INSERT INTO reported_errors(datetime, priority, generating_script, error) VALUES('" . date("Y-m-d H:i:s") .
  	"','10','programs/basic-file-operations/create-new-item.php','Could not successfully touch a new file on the filesystem.");
    exit ("[Error] Could not create the file on the filesystem.");
  }
  
  if (!$database->exec("INSERT INTO files(gID, uID, location, mime, hashName, name, size, dateIntroduced) " .
	"VALUES('" . $global_id . "','" . $user_id . "','" . $destination_information["gID"] . "','" .
	$filetype_information["mime"] . "','" . $file_hash . "','" . $name . "','0','" .
	date("Y-m-d H:i:s") . "')"))
  {
	  $database->exec("DELETE FROM global_ids WHERE gID='" . $global_id . "'");
	  unlink(IMPEC_PATH . "/files/" . $file_hash);
	  exit ("[Error] Could not create the item in the database.");
  }
  
  require_once (IMPEC_PATH . "/icon-generation-scripts/create-icon.php");
  createFileIcon($file_hash, $filetype_information["mime"]);
	
} else
{
  $database->exec("DELETE FROM global_ids WHERE gID='" . $global_id . "'");
  $database->exec("INSERT INTO reported_errors(datetime, priority, generating_script, error) VALUES('" . date("Y-m-d H:i:s") .
  	"','10','programs/basic-file-operations/create-new-item.php','Attempted to create an item listed as having the gID type of " .
  	$filetype_information["gIDType"] . ", which is not supported here.");
  exit ("[Error] Unable to programmatically create the file, as the specified gID type is not supported yet.");
}

exit("[Success] " . $global_id);
?>
