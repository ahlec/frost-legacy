function BasicFileOperations() { };
BasicFileOperations.dragDropUpload = null;
BasicFileOperations.initialize = function()
{
  BasicFileOperations.dragDropUpload = document.createElement("input");
  BasicFileOperations.dragDropUpload.type = "file";
  BasicFileOperations.dragDropUpload.style.position = "absolute";
  BasicFileOperations.dragDropUpload.style.left = "0px";
  BasicFileOperations.dragDropUpload.style.top = "0px";
  BasicFileOperations.dragDropUpload.style.zIndex = 900;
//  document.body.appendChild(BasicFileOperations.dragDropUpload);
};
BasicFileOperations.initialize();

BasicFileOperations.rename = function(context_info)
{
  var renameFrame = new Frame("rename");
  renameFrame.load("programs/basic-file-operations/rename-screen.php", context_info.gid);
}

BasicFileOperations.moveToTrash = function(context_info)
{
  Prompt("Are you sure you wish to move this item to the trash?", "Move to trash?", [
		{text: 'Yes', colour:'green', action: function() { moveToTrash(context_info.gid); }},
		{text: 'No', colour:'red', action: function() {} }
	]);
}
BasicFileOperations.createFolder = function(target_information)
{
  var createFolderFrame = new Frame("create-folder");
  createFolderFrame.load("programs/basic-file-operations/create-folder-screen.php", target_information.gid, function()
  {
    document.getElementById('new-folder-name').focus();
  });
};
BasicFileOperations.addFile = function(context_info)
{
  var addFileFrame = new Frame("add-file");
  addFileFrame.load("programs/basic-file-operations/add-file-screen.php", context_info.gid);
};
BasicFileOperations.download = function(context_info)
{
  window.location = web_path + "/files/" + context_info.gid;
}


BasicFileOperations.createNew = function(context_info)
{
  var createNewFrame = new Frame("create-new-item");
  createNewFrame.load("programs/basic-file-operations/create-new-item-screen.php", context_info.gid);
}

/* Main Functionality */
function rename(gid, new_name)
{
  document.getElementById("rename-button").disabled = true;
  document.getElementById("rename-cancel").disabled = true;
  executeAJAX("programs/basic-file-operations/rename.php?gid=" + gid + "&newName=" + new_name, function process(results)
  {
    if (results == "success")
    {
      var iconItems = IconItem.getItemsByGId(gid);
      for (var index = 0; index < iconItems.length; index++)
        iconItems[index].setName(new_name);
      Frame.getByHandle("rename").close();
      return;
    }
    alert(results);
    document.getElementById("rename-button").disabled = false;
    document.getElementById("rename-cancel").disabled = false;
  });
}

function moveToTrash(gid)
{
  executeAJAX("programs/basic-file-operations/move-to-trash.php?gid=" + gid, function process(results)
  {
    if (results == 'success')
    {
      var iconItems = IconItem.getItemsByGId(gid);
      for (var index = 0; index < iconItems.length; index++)
        iconItems[index].destroy();
      Trash.setContentsCount(Trash.contentsCount + 1);
      Trash.reloadFrame();
      return;
    }
    alert(results);
  });
}
function uploadFileResults(results)
{
  if (results.length == 0)
    return;
  if (results.indexOf("[Success]") > -1)
  {
    var gID = results.replace("[Success] ", "");
    executeAJAX("scripts/get-item.php?gid=" + gID, function process(newItem)
    {
      newItem = eval("(" + newItem + ")");
      var destinations = LocationContents.getByGId(newItem.location);
      for (var index = 0; index < destinations.length; index++)
        destinations[index].addItem(newItem);
      Frame.getByHandle("add-file").close();
    });
  } else
    alert("!" + results);
}
function createFolder(destination, name)
{
  executeAJAX("programs/basic-file-operations/create-folder.php?destination=" + destination + "&name=" +
  	encodeURIComponent(name), function(results)
  	{
  	  if (results.indexOf("[Success]") > -1)
  	  {
  	    var gID = results.replace("[Success] ", "");
  	    executeAJAX("scripts/get-item.php?gid=" + gID, function(newItem)
  	    {
  	      newItem = eval("(" + newItem + ")");
  	      var destinations = LocationContents.getByGId(newItem.location);
  	      for (var index = 0; index < destinations.length; index++)
  	        destinations[index].addItem(newItem);
  	      Frame.getByHandle("create-folder").close();
  	    });
  	  } else
  	    alert(results);
  	});
};
function moveItem(gid, destination)
{
  executeAJAX("programs/basic-file-operations/move.php?gid=" + gid + "&destination=" + destination, function(results)
  {
    if (results == 'success')
    {
      var iconItems = IconItem.getItemsByGId(gid);
      for (var index = 0; index < iconItems.length; index++)
        iconItems[index].destroy();
      executeAJAX("scripts/get-item.php?gid=" + gid, function(newItem)
      {
        newItem = eval("("+ newItem + ")");
        var destinations = LocationContents.getByGId(newItem.location);
        for (var addIndex = 0; addIndex < destinations.length; addIndex++)
          destinations[addIndex].addItem(newItem);
      });
      return;
    }
    alert(results);
  });
}

function createNewItem(destination, mime, name)
{
  executeAJAX("programs/basic-file-operations/create-new-item.php?destination=" + destination.toString() + "&mime=" +
  	encodeURIComponent(mime) + "&name=" + encodeURIComponent(name), function(results)
  	{
  	  if (results.indexOf("[Success]") > -1)
  	  {
  	    var gID = results.replace("[Success] ", "");
  	    executeAJAX("scripts/get-item.php?gid=" + gID.toString(), function(newItem)
  	    {
  	      newItem = eval("(" + newItem + ")");
  	      var destinations = LocationContents.getByGId(newItem.location);
  	      for (var index = 0; index < destinations.length; index++)
  	        destinations[index].addItem(newItem);
  	      Frame.getByHandle("create-new-item").close();
  	    });
  	  } else
  	    alert(results);
  	});
}