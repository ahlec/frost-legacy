<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_GET["gid"]) || !isset($_GET["newName"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if ($database->querySingle("SELECT count(*) FROM global_ids LEFT JOIN users ON global_ids.uID = users.uID " .
	"WHERE gID='" . $database->escapeString($_GET["gid"]) . "' AND uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "'") == 0)
  exit ("The target no longer exists or is not owned by you.");

$targetType = $database->querySingle("SELECT type FROM global_ids WHERE gID='" . $database->escapeString($_GET["gid"]) .
	"' LIMIT 1");
$new_name = $database->escapeString($_GET["newName"]);

$success = false;
switch ($targetType)
{
  case "location": $success = $database->exec("UPDATE locations SET name='" . $new_name . "' WHERE gID='" .
	$database->escapeString($_GET["gid"]) . "'"); break;
  case "file": $success = $database->exec("UPDATE files SET name='" . $new_name . "' WHERE gID='" .
	$database->escapeString($_GET["gid"]) . "'"); break;
}

if (!$success)
  exit ("[Error] There was an error encountered when modifying the database records.");
exit("success");
?>
