<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_GET["gid"]) || !isset($_GET["destination"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if ($database->querySingle("SELECT count(*) FROM global_ids LEFT JOIN users ON global_ids.uID = users.uID " .
	"WHERE gID='" . $database->escapeString($_GET["gid"]) . "' AND uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "'") == 0)
  exit ("The target no longer exists or is not owned by you.");

$targetType = $database->querySingle("SELECT type FROM global_ids WHERE gID='" . $database->escapeString($_GET["gid"]) .
	"' LIMIT 1");
	
if ($database->querySingle("SELECT count(*) FROM global_ids LEFT JOIN users ON global_ids.uID = users.uID " .
	"WHERE gID='" . $database->escapeString($_GET["destination"]) . "' AND (uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' OR global_ids.uID = 0) AND global_ids.type='location'") == 0)
  exit ("The destination does not exist, does not belong to you, or is not a location.");

$success = false;
switch ($targetType)
{
  case "location": $success = $database->exec("UPDATE locations SET previousLocation = parent_location, parent_location='" .
  	$database->escapeString($_GET["destination"]) . "' WHERE gID='" . $database->escapeString($_GET["gid"]) . "'"); break;
  case "file": $success = $database->exec("UPDATE files SET previousLocation = location, location='" .
  	$database->escapeString($_GET["destination"]) . "', " .
	"dateLastUpdated='" . date("Y-m-d G:i:s") . "'  WHERE gID='" . $database->escapeString($_GET["gid"]) . "'"); break;
}

if (!$success)
  exit ("[Error] There was an error encountered when modifying the database records.");
exit("success");
?>
