<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/files.php");
@session_start();

if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_FILES["uploaded-file"]) || !isset($_GET["destination"]))
  exit ("[Error] Parameters were not fully passed.");

if ($_FILES["uploaded-file"]["error"] != 0)
  exit("[Error] There was an error uploading the file. Please try again");

$temp_name = preg_replace("/[^0-9a-zA-Z]/","", $_FILES["uploaded-file"]["tmp_name"]);

if (!file_exists(DOCUMENT_ROOT . "/programs/basic-file-operations/temp"))
  mkdir(DOCUMENT_ROOT . "/programs/basic-file-operations/temp");
if (!move_uploaded_file($_FILES["uploaded-file"]["tmp_name"], DOCUMENT_ROOT . "/programs/basic-file-operations/temp/" . $temp_name))
  exit ("[Error] Could not move the uploaded file to the processing directory.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$destination_information = $database->querySingle("SELECT gID FROM locations LEFT JOIN users ON users.uID = locations.uID WHERE " .
	"(locations.uID = '0' OR uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "') AND locations.gID='" .
	$database->escapeString($_GET["destination"]) . "' LIMIT 1", true);

if ($destination_information === false)
{
  unlink(DOCUMENT_ROOT . "/programs/basic-file-operations/temp/" . $_FILES["uploaded-file"]["tmp_name"]);
  exit ("[Error] The destination does not exist or is not your property.");
}

$user_id = $database->querySingle("SELECT uID FROM users WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' LIMIT 1");
$file_name = $database->escapeString(addslashes($_FILES["uploaded-file"]["name"]));
$file_type = $database->escapeString($_FILES["uploaded-file"]["type"]);
$size = $database->escapeString($_FILES["uploaded-file"]["size"]);
do
{
  $file_hash = randomString();
} while ($database->querySingle("SELECT count(*) FROM files WHERE hashName='" .
	$database->escapeString($file_hash) . "'") != 0);
$contents = file_get_contents(DOCUMENT_ROOT . "/programs/basic-file-operations/temp/" . $temp_name);
unlink(DOCUMENT_ROOT . "/programs/basic-file-operations/temp/" . $temp_name);

$database->exec("INSERT INTO global_ids(uID, type) VALUES('" . $user_id . "','file')");
$global_id = $database->getLastAutoInc();
if ($global_id === false)
  exit ("[Error] Could not register a unique global ID in the database during upload.");

if (!$database->exec("INSERT INTO files(gID, uID, location, mime, hashName, name, size, dateIntroduced) " .
	"VALUES('" . $global_id . "','" . $user_id . "','" . $destination_information["gID"] . "','" .
	$file_type . "','" . $file_hash . "','" . $file_name . "','" . $size . "','" .
	date("Y-m-d H:i:s") . "')"))
{
  $database->exec("DELETE FROM global_ids WHERE gID='" . $global_id . "'");
  exit ("[Error] Could not upload the file to the database.");
}

file_put_contents(IMPEC_PATH . "/files/" . $file_hash, $contents);

require_once (IMPEC_PATH . "/icon-generation-scripts/create-icon.php");
createFileIcon($file_hash, $file_type);

/*$generation_script = $database->querySingle("SELECT icon FROM filetype_associations WHERE mime='" . $file_type . "' AND icon_generated='" .
	"1' LIMIT 1");
if ($generation_script != false && $generation_script != "")
{
  $globalId = $global_id;
  require_once(IMPEC_PATH . "/icon-generation-scripts/" . $generation_script);
  // generate icon
}*/

exit("[Success] " . $global_id);
?>
