<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not logged in.");
if (!isset($_GET["type"]) || !isset($_GET["identity"]))
  exit ("[Error] Parameters were not fully passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if ($_GET["type"] != "folder" && $_GET["type"] != "file")
  exit ("[Error] Invalid valid ('" . $_GET["type"] . "') passed to renaming script.");

if (($_GET["type"] == "file" && $database->querySingle("SELECT count(*) FROM files JOIN users ON users.uID = files.uID WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND id='" . $database->escapeString($_GET["identity"]) .
	"'") == 0) || ($_GET["type"] == "folder" && $database->querySingle("SELECT count(*) FROM folders JOIN users ON users.uID = " .
	"folders.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND ref='" .
	$database->escapeString($_GET["identity"]) . "'") == 0))
  exit ("[Error] The provided " . $_GET["type"] . " of identity " . $_GET["identity"] . " does not exist or does not belong to you.");

$success = ($_GET["type"] == "file" ? $database->exec("UPDATE files SET location='trash' WHERE id='" .
	$database->escapeString($_GET["identity"]) . "'") : $database->exec("UPDATE folders SET location='trash' WHERE ref='" . 
	$database->escapeString($_GET["identity"]) . "'"));
if (!$success)
  exit ("[Error] There was an error encountered when modifying the database records.");
exit("success");
?>
