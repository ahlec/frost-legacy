function Folder() { }
Folder.open = function(context_info)
{
  if (Frame.getByHandle("folder-" + context_info.gid))
  {
    Frame.getByHandle("folder-" + context_info.gid).focus();
    return;
  }
  var folderFrame = new Frame("folder-" + context_info.gid);
  executeAJAX("scripts/get-location.php?identity=" + context_info.gid, function loaded(locationInfo)
  {
    var locationJSON;
    try
	{
	  locationJSON = eval("(" + locationInfo + ")");
	} catch(jsonException)
	{
	  alert(locationInfo);
	  return;
	}
    folderFrame.getFrameContent().contextHandles = locationJSON.contextHandles;
    folderFrame.getFrameContent().base = {};
    folderFrame.getFrameContent().base.getGID = function() { return locationJSON.gID; };
    folderFrame.load("programs/folder/frame.php", context_info.gid, function()
    {
      var contents = new LocationContents(locationJSON.gID, locationJSON.contents);
      contents.applyTo(folderFrame.getFrameContent());
      folderFrame.focus();
    }, false);
  });
};

/*
  this.open = function()
  {
    if (Frame.getByHandle("folder-" + id))
    {
      Frame.getByHandle("folder-" + id).focus();
      return;
    }
    var folderFrame = new Frame("folder-" + id);
    folderFrame.load("screens/folder.php", id, thisFolder.fillFrame, false);
  };

  this.fillFrame = function(frame)
  {
    for (var index = 0; index < location.length; index++)
    {
      var locationLink = document.createElement("div");
      locationLink.innerHTML = location[index].name;
      if (frame.sidebar.hasChildNodes())
        frame.sidebar.insertBefore(locationLink, frame.sidebar.firstChild);
      else
        frame.sidebar.appendChild(locationLink);
    }
    return;
    executeAJAX("scripts/get-location-contents.php?location=" + id, function displayContents(contents)
	{
	  var contents = eval("(" + contents + ")");
	  for (var index = 0; index < contents.contents.length; index++)
	  {
	    if (contents.contents[index].type == "folder")
	    {
		var itemFolder = new Folder();
		itemFolder.load(contents.contents[index].id,
		  function add()
		  {
			var iconItem = new IconItem(itemFolder.getIcon(), itemFolder.getName(),
				itemFolder);
			iconItem.onExecute = itemFolder.open;
			frameContent.appendChild(iconItem.container);
		  });
	    } else
	    {
		var itemFile = new File();
		itemFile.load(contents.contents[index].id,
		  function add()
		  {
			var iconItem = new IconItem(Files.getInformation(itemFile.getType())[1],
				itemFile.getName(), itemFile);
			frameContent.appendChild(iconItem.container);
		  });
	    }
	  }
	});
  };
  this.load = function(folder_id, on_load)
  {
    executeAJAX("scripts/get-folder-information.php?identity=" + folder_id, function process(results)
    {
      if (results == "")
      {
        alert("Folder does not exist or does not belong to you.");
        return;
      }
      var folderInformation = eval("(" + results + ")");
      id = folderInformation.id;
      name = folderInformation.name;
      icon = folderInformation.icon;
      location = folderInformation.location;
      executeAJAX("scripts/get-context-menu.php?target_type=folder", function finish(contextMenu)
      {
        rawContextMenu = eval("(" + contextMenu + ")");
        if (on_load)
          on_load();
      });
    });
  };
}
*/
