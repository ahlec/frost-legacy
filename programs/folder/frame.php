<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["parameter"]) ||
	$database->querySingle("SELECT count(*) FROM locations LEFT JOIN users ON locations.uID = users.uID WHERE " .
	"(locations.uID = '0' OR users.uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "') AND gID='" .
	$database->escapeString($_GET["parameter"]) . "'") == 0)
  exit ("This folder does not exist, or you are not the owner of it.");

$folder_information = $database->querySingle("SELECT gID, name FROM locations WHERE gID='" .
	$database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'absolute',\n";
  echo "  top:'10px',\n";
  echo "  left:'10px',\n";
  echo "  width:'550px',\n";
  echo "  height:'500px',\n";
  echo "  sidebar:'left',\n";
  echo "  canClose:'true',\n";
  echo "  title:'" . $folder_information["name"] . "',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  icon:'',\n";
  echo "  icon16:'programs/folder/icon16.png'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

?>
