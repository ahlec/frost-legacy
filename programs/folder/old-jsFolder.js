function Folder()
{
  var id = null;
  var name = null;
  var icon = null;
  var location = null;
  var rawContextMenu = null;
  var thisFolder = this;

  this.getId = function() { return id; };
  this.getName = function() { return name; };
  this.getIcon = function() { return icon; };
  this.getLocation = function() { return location; };
  this.getRawContextMenu = function() { return rawContextMenu; };

  this.open = function()
  {
    if (Frame.getByHandle("folder-" + id))
    {
      Frame.getByHandle("folder-" + id).focus();
      return;
    }
    var folderFrame = new Frame("folder-" + id);
    folderFrame.load("screens/folder.php", id, thisFolder.fillFrame, false);
  };

  this.fillFrame = function(frame)
  {
    for (var index = 0; index < location.length; index++)
    {
      var locationLink = document.createElement("div");
      locationLink.innerHTML = location[index].name;
      if (frame.sidebar.hasChildNodes())
        frame.sidebar.insertBefore(locationLink, frame.sidebar.firstChild);
      else
        frame.sidebar.appendChild(locationLink);
    }
    return;
    executeAJAX("scripts/get-location-contents.php?location=" + id, function displayContents(contents)
	{
	  var contents = eval("(" + contents + ")");
	  for (var index = 0; index < contents.contents.length; index++)
	  {
	    if (contents.contents[index].type == "folder")
	    {
		var itemFolder = new Folder();
		itemFolder.load(contents.contents[index].id,
		  function add()
		  {
			var iconItem = new IconItem(itemFolder.getIcon(), itemFolder.getName(),
				itemFolder);
			iconItem.onExecute = itemFolder.open;
			frameContent.appendChild(iconItem.container);
		  });
	    } else
	    {
		var itemFile = new File();
		itemFile.load(contents.contents[index].id,
		  function add()
		  {
			var iconItem = new IconItem(Files.getInformation(itemFile.getType())[1],
				itemFile.getName(), itemFile);
			frameContent.appendChild(iconItem.container);
		  });
	    }
	  }
	});
  };
  this.load = function(folder_id, on_load)
  {
    executeAJAX("scripts/get-folder-information.php?identity=" + folder_id, function process(results)
    {
      if (results == "")
      {
        alert("Folder does not exist or does not belong to you.");
        return;
      }
      var folderInformation = eval("(" + results + ")");
      id = folderInformation.id;
      name = folderInformation.name;
      icon = folderInformation.icon;
      location = folderInformation.location;
      executeAJAX("scripts/get-context-menu.php?target_type=folder", function finish(contextMenu)
      {
        rawContextMenu = eval("(" + contextMenu + ")");
        if (on_load)
          on_load();
      });
    });
  };
}
