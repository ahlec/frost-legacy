<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");
if (!isset($_GET["parameter"]))
  exit ("Parameters were not passed.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'200px',\n";
  echo "  height:'300px',\n";
  echo "  title:'Open with...',\n";
  echo "  icon:'programs/program-selector/icon-openWith.png',\n";
  echo "  modal:'true',\n";
  echo "  canClose:'true',\n";
  echo "  onTaskbar:'false'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

$all_applications = $database->query("SELECT program_id, handle, title, icon FROM programs WHERE isApplication='1'");
while ($application = $all_applications->fetchArray())
{
  echo "<input type=\"button\" value=\"" . mb_addslashes($application["title"]) . "\" onClick=\"Frame.getByHandle('open-with').close();" .
  	$application["handle"] . ".open({gid:" . $_GET["parameter"] . "});\" style=\"width:100%;display:block;\" />\n";
}
?>
