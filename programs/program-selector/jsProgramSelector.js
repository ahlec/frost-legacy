function ProgramSelector() { };

ProgramSelector.openWith = function(context_info)
{
  var openWithFrame = new Frame("open-with");
  openWithFrame.load("programs/program-selector/open-with.php", context_info.gid);
}