<?php
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$image_information = $database->querySingle("SELECT gID, location, mime, hashName, name, size FROM files JOIN " .
	"users ON users.uID = files.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' AND gID='" . $database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);
if (strpos($image_information["mime"], "image/") !== 0)
  exit ("[Error] Attempted to load an invalid filetype as an image.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  width:'500px',\n";
  echo "  height:'500px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'" . addslashes($image_information["name"]) . "',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  hasTabs:'true',\n";
  echo "  icon16:'programs/image-viewer/icon16.png',\n";
  echo "  icon:'programs/image-viewer/icon-imageViewer.png',\n";
  echo "  toolbar: []\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

$imagesize = getimagesize(IMPEC_PATH . "/files/" . $image_information["hashName"]);
$scale = 1.0;
if ($imagesize[0] >= 500 || $imagesize[1] >= 400)
{
  $scalex = 490 / $imagesize[0];
  $scaley = 430 / $imagesize[1];
  if ($scalex <= $scaley)
    $scale = $scalex;
  else
    $scale = $scaley;
  /*if ($imagesize[0] > $imagesize[1])
  {
    $scale = 490 / $imagesize[0];
  } else
    $scale = 430 / $imagesize[1];*/
}

echo "<div style=\"width:500px;height:440px;overflow:hidden;background-color:#CFCFCF;" .
	"text-align:center;margin-bottom:5px;border-bottom:1px solid black;\">\n";
echo "  <img src=\"" . URL_ROOT . "/files/" . $image_information["gID"] . "\" style=\"width:" . 
	($scale * $imagesize[0]) . "px;margin:4px;border:1px solid #2E2E2E;height:" . ($scale * $imagesize[1]) . "px;background:url('" .
	URL_ROOT . "/programs/image-viewer/background.png');\" />";
echo "</div>\n";
echo "<b>Actual Dimensions:</b> " . $imagesize[0] . "x" . $imagesize[1] . " &ndash; <b>Scale:</b>" . $scale;

?>
