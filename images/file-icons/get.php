<?php
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Content-type: image/png");
require_once ("../../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["gid"]))
  exit(file_get_contents("unknown.png"));
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
$file_information = $database->querySingle("SELECT hashName, icon, icon_generated FROM files JOIN users ON users.uId = " .
	"files.uID LEFT JOIN filetype_associations " .
	"ON filetype_associations.mime = files.mime WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND files.gID='" .
	$database->escapeString($_GET["gid"]) . "' AND icon_generated='1'", true);

if ($file_information === false)
  exit (file_get_contents("unknown.png"));
if ($file_information["icon_generated"] == 0)
  exit (file_get_contents($file_information["icon"]));

exit(file_get_contents(IMPEC_PATH . "/file-icons/" . $file_information["hashName"]));
?>
