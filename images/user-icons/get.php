<?php
header("Content-type: image/png");
require_once ("../../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["uid"]))
  exit(file_get_contents("no-user-icon.png"));
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
$icon_information = $database->querySingle("SELECT uHash, hasIcon FROM users WHERE uID='" .
	$database->escapeString($_GET["uid"]) . "' LIMIT 1", true);

if ($icon_information === false || $icon_information["hasIcon"] == "0")
  exit (file_get_contents("no-user-icon.png"));
exit(file_get_contents(IMPEC_PATH . "/user-icons/" . $icon_information["uHash"]));
?>
