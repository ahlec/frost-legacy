<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["file"]))
  exit("[Error]");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if ($database->querySingle("SELECT count(*) FROM files JOIN users ON users.uId = files.uID WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND files.gID='" .
	$database->escapeString($_GET["file"]) . "'") == 0)
  exit ("[Error]");

$file_information = $database->querySingle("SELECT name, files.mime, extension, size, hashName FROM files JOIN " .
	"users ON users.uID = " .
	"files.uID JOIN filetype_associations ON filetype_associations.mime = files.mime WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND files.gID='" . $database->escapeString($_GET["file"]) .
	"' LIMIT 1", true);

header("Content-length: " . $file_information["size"]);
header("Content-type: " . $file_information["type"]);
$filename = preg_replace("/(\\s|,)/", "_", $file_information["name"]) . "." . $file_information["extension"];
header("Content-Disposition: attachment; filename = " . $filename);
exit(file_get_contents(IMPEC_PATH . "/files/" . $file_information["hashName"]));
?>
