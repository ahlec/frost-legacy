<?
/*      THEME INFORMATION
Theme Name: Island Whale
Date Created: 30 August 2009
Date Last Modified: 30 August 2009
Primary Colors: Blue, Green, Tan
*/


/*      DESKTOP COLORS (2 Colors)
*/
$desktopBackground = '3C4D5A';
$desktopBackgroundBorder = '6D7764';

/*      WINDOW COLORS (11 Colors)
0. [Window] Background Color
1. [Window] Border Color
2. [Window] Font Color
3. [Window] Link Color
4. [Window Title Bar] Background Color
5. [Window Title Bar] Font Color
6. [Window Title Button] Background Color
7. [Window Title Button] Border Color
8. [Window Title Button] Font Color
9. [Unselected File] Hover Color
10. [Selected File] Background Color
*/
$windowColors = array(
0=> '93b0be',
1=> '101418',
2=> '263444',
3=> '263444',
4=> '547593',
5=> 'ffffff',
6=> 'B1BBD2',
7=> '64718F',
8=> '8B919E',
9=> 'c7d8d1',
10=> 'b0d8e9');

/*      UNFOCUSED WINDOW COLORS (7 Colors)
1. [Window] Background Color
2. [Window] Border Color
3. [Window] Font Color
4. [Window] Link Color
5. [Window Title Bar] Background Color
6. [Window Title Bar] Border Color
7. [Window Title Bar] Font Color
*/
$unfocusedWindowColors = array();

/*      TASK BAR COLORS (12 Colors)
0. [Task Bar] Background Color
1. [Task Bar] Border Color
2. [Task Bar Item] Background Color
3. [Task Bar Item] Border Color
4. [Task Bar Item] Font Color
5. [Unfocused Task Bar Item] Background Color
6. [Unfocused Task Bar Item] Border Color
7. [Unfocused Task Bar Item] Font Color
8. [User Menu] Background Color
9. [User Menu] Border Color
10. [User Menu] Font Color
11. [User Menu] Hover Color
*/
$taskbarColors = array(
0=> 'f0f5d8',
1=> '8c8353',
2=> 'def8f9',
3=> '849ca2',
4=> '849ca2',
5=> '',
6=> '',
7=> '',
8=> 'def8f9',
9=> '849ca2',
10=> '849ca2',
11=> 'e2f1f0');

/*      ACTIONS BAR COLORS (5 Colors)
0. [Action Bar] Background Color
1. [Action Bar] Border Color
2. [Action Bar Item] Background Color
3. [Action Bar Item] Border Color
4. [Action Bar Item] Font Color
*/
$actionsbarColors = array(
0=> 'd5eaa9',
1=> '808e54',
2=> 'adc17f',
3=> '606728',
4=> '535f27');

/*      ICON COLORS (6 Colors)
1. [Regular Icon] Background Color
2. [Regular Icon] Border Color
3. [Regular Icon] Font Color
4. [Special Icon] Background Color
5. [Special Icon] Border Color
6. [Special Icon] Font Color
*/
$iconColors = array();
?>
