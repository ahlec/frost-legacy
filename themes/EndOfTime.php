<?
/*      THEME INFORMATION
Theme Name: End Of Time
Date Created: 30 August 2009
Date Last Modified: 30 August 2009
Primary Colors: COLOR ONE, COLOR TWO, COLOR THREE, [COLOR FOUR (optional)]
*/

/*      WINDOW COLORS (11 Colors)
0. [Window] Background Color
1. [Window] Border Color
2. [Window] Font Color
3. [Window] Link Color
4. [Window Title Bar] Background Color
5. [Window Title Bar] Font Color
6. [Window Title Button] Background Color
7. [Window Title Button] Border Color
8. [Window Title Button] Font Color
9. [Unselected File] Hover Color
10. [Selected File] Background Color
*/
$windowColors = array(
0=> 'd8caa1',
1=> '2a1506',
2=> '35220d',
3=> '35220d',
4=> 'bba16d',
5=> '35220d',
6=> '8f7c51',
7=> '7d6437',
8=> '2a1506',
9=> 'e1d6ab',
10=> 'f5e8c1');

/*      UNFOCUSED WINDOW COLORS (7 Colors)
1. [Window] Background Color
2. [Window] Border Color
3. [Window] Font Color
4. [Window] Link Color
5. [Window Title Bar] Background Color
6. [Window Title Bar] Border Color
7. [Window Title Bar] Font Color
*/
$unfocusedWindowColors = array();

/*      TASK BAR COLORS (12 Colors)
0. [Task Bar] Background Color
1. [Task Bar] Border Color
2. [Task Bar Item] Background Color
3. [Task Bar Item] Border Color
4. [Task Bar Item] Font Color
5. [Unfocused Task Bar Item] Background Color
6. [Unfocused Task Bar Item] Border Color
7. [Unfocused Task Bar Item] Font Color
8. [User Menu] Background Color
9. [User Menu] Border Color
10. [User Menu] Font Color
11. [User Menu] Hover Color
*/
$taskbarColors = array(
0=> 'EE9A49',
1=> '8B4500',
2=> 'FFDAB9',
3=> '8B4500',
4=> '000000',
5=> '',
6=> '',
7=> '',
8=> '',
9=> '',
10=> '',
11=> '');

/*      ACTIONS BAR COLORS (5 Colors)
0. [Action Bar] Background Color
1. [Action Bar] Border Color
2. [Action Bar Item] Background Color
3. [Action Bar Item] Border Color
4. [Action Bar Item] Font Color
*/
$actionsbarColors = array(
0=> '',
1=> '',
2=> '',
3=> '',
4=> '');

/*      ICON COLORS (6 Colors)
1. [Regular Icon] Background Color
2. [Regular Icon] Border Color
3. [Regular Icon] Font Color
4. [Special Icon] Background Color
5. [Special Icon] Border Color
6. [Special Icon] Font Color
*/
$iconColors = array();
?>
