<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["location"]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
$files = $database->query("SELECT id, name, size, type FROM files JOIN users ON users.uID = files.uID " .
	"WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND location='" .
	$database->escapeString($_GET["location"]) . "' ORDER BY name");
$folders = $database->query("SELECT foldername, ref, canModify, icon FROM folders JOIN users ON " .
	"users.uID = folders.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' AND location='" . $database->escapeString($_GET["location"]) . "' ORDER BY foldername");
if ($files === false || $folders === false)
  exit ("");
echo "{contents:[";
$folders_out = 0;
while ($folder = $folders->fetchArray())
{
  echo "  {\n";
  echo "     id: '" . $folder["ref"] . "',\n";
  echo "     name: '" . $folder["foldername"] . "',\n";
  echo "     type: 'folder',\n";
  echo "     canModify: '" . $folder["canModify"] . "',\n";
  echo "     icon: '" . $folder["icon"] . "'}";
  if ($folders_out < $folders->numberRows() - 1 || $files->numberRows() > 0)
    echo ",";
  echo "\n";
  $folders_out++;
}
$files_out = 0;
while ($file = $files->fetchArray())
{
  echo "  {\n";
  echo "    id: '" . $file["id"] . "',\n";
  echo "    name: '" . $file["name"] . "',\n";
  echo "    size: '" . $file["size"] . "',\n";
  echo "    type: '" . $file["type"] . "'\n";
  echo "  }";
  if ($files_out < $files->numberRows() - 1)
    echo ",";
  echo "\n";
  $files_out++;
}
echo "]}";
?>
