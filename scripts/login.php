<?php
require_once ("../framework/config.php");
require_once ("../framework/database.php");
@session_start();
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are already logged in.");
if (!isset($_GET["username"]) || !isset($_GET["password"]))
  exit ("[Error] Not enough valid parameters.");
if (preg_match("/[^0-9a-zA-Z ]/", $_GET["username"]))
  exit ("[Error] Invalid characters in entered username.");
$login_results = $database->querySingle("SELECT uHash FROM users WHERE username LIKE '" .
	$database->escapeString($_GET["username"]) . "' AND password='" . 
	$database->escapeString($_GET["password"]) . "' LIMIT 1");
if ($login_results === FALSE)
  exit ("[Error] Invalid username and password combination.");
$database->exec("UPDATE users SET lastLogin='" . date("Y-m-d H:i:s") . "' WHERE uHash='" . $login_results . "'");
$_SESSION[DEITLOFF_SESSION] = $login_results;
exit ("success");
?>
