<?php
require_once ("../framework/config.php");
require_once ("../framework/database.php");
@session_start();
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("[Error] You are not currently logged in.");
unset($_SESSION[DEITLOFF_SESSION]);
exit ("success");
?>
