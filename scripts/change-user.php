<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You are not logged in.");
if (!isset($_GET["aspect"]))
  exit ("Valid parameters were not passed.");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
switch ($_GET["aspect"])
{
  case "theme":
  {
    if (!isset($_GET["id"]))
      exit("Not all parameters were passed through.");
    if ($database->querySingle("SELECT count(*) FROM themes WHERE ID='" .
	$database->escapeString($_GET["id"]) . "'") == 0)
      exit("Invalid parameter value.");
    if (!$database->exec("UPDATE users SET theme='" . $database->escapeString($_GET["id"]) .
	"' WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "'"))
      exit ("Could not update the database.");
    exit("success");
  }
  default: exit ("Invalid change aspect.");
}
?>
