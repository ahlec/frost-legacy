<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$associations = $database->query("SELECT mime, icon, program FROM filetype_associations"); 
if ($associations === false)
  exit ("");
echo "{associations:[";
$associations_out = 0;
while ($association = $associations->fetchArray())
{
  echo "  {\n";
  echo "     mime: '" . $association["mime"] . "',\n";
  echo "     icon: '" . $association["icon"] . "',\n";
  echo "     program: '" . $association["program"] . "'}";
  if ($associations_out < $associations->numberRows() - 1)
    echo ",";
  echo "\n";
  $associations_out++;
}
echo "]}";
?>
