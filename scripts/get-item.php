<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You are not logged in.");
if (!isset($_GET["gid"]))
  exit("Parameters were not passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

if ($database->querySingle("SELECT count(*) FROM global_ids LEFT JOIN users ON global_ids.uID = users.uID WHERE " .
	"(uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' OR global_ids.uID='0') AND global_ids.gID='" .
	$database->escapeString($_GET["gid"]) . "'") == 0)
  exit ("Item does not exist or does not belong to you.");

$item_information = $database->querySingle("SELECT global_ids.gID, global_ids.type AS \"type\", locations.handle AS \"location_handle\", " .
	"IFNULL(files.name, locations.name) AS \"name\", locations.type AS \"location_type\", files.size AS \"file_size\", " .
	"files.mime AS \"file_mime\", IFNULL(locations.custom_icon, filetype_associations.icon) AS \"icon\", files.hashName AS \"file_hash\", " .
	"IF(filetype_associations.icon_generated IS NULL, 0, filetype_associations.icon_generated) AS \"icon_generated\", " .
	"filetype_associations.program, IFNULL(files.location, locations.parent_location) AS \"location\" " .
	"FROM global_ids LEFT JOIN files ON (files.gID = global_ids.gID AND global_ids.type='file') " .
	"LEFT JOIN locations ON (locations.gID = global_ids.gID AND global_ids.type='location') LEFT JOIN filetype_associations " .
	"ON IF(global_ids.type = 'file', files.mime, LOWER(IFNULL(locations.handle, locations.type))) = " .
	"filetype_associations.mime LEFT JOIN users ON global_ids.uID = users.uID " .
	"WHERE global_ids.gID='" . $database->escapeString($_GET["gid"]) . "' AND (users.uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' OR global_ids.uID='0') LIMIT 1", true);

echo "{";
echo "  type:'" . $item_information["type"] . "',\n";
echo "  location:'" . $item_information["location"] . "',\n";
echo "  icon:'";
  if ($item_information["icon_generated"] == "1" && $item_information["type"] == "file")
    echo "get.php?gid=" . $item_information["gID"];
  else
  {
    if ($item_information["type"] == "location")
    {
      if ($item_information["location_handle"] == "trash")
        echo "trash-empty.png";
      else if ($item_information["location_type"] == "Folder")
        echo "folder.png";
    } else
      echo $item_information["icon"];
  }
echo "',\n";
echo "  gID:'" . $item_information["gID"] . "',\n";
if ($item_information["location_handle"] != "")
  echo "  handle:'" . $item_information["location_handle"] . "',\n";
echo "  name:'" . addslashes($item_information["name"]) . "',\n";
if ($item_information["file_size"] != "")
  echo "  size:'" . $item_information["file_size"] . "',\n";
if ($item_information["file_mime"] != "")
  echo "  mime:'" . $item_information["file_mime"] . "',\n";
if ($item_information["location"] == "2")
  echo "  contextHandles:['trashItem']\n";
else
{
  echo "  defaultProgram:'" . $item_information["program"] . "',\n";
  echo "  contextHandles:['" . $item_information["type"] . "'" . ($item_information["type"] == "file" ? ",'" . $item_information["file_mime"] . "'" :
	"") . ($item_information["location_type"] == "Special" && $item_information["location_handle"] != "" ?
	",'" . addslashes($item_information["location_handle"]) . "'" : ($item_information["type"] == "location" ?
	",'" . strtolower($item_information["location_type"]) . "'" : "")) . "]\n";
}
echo "}";
?>
