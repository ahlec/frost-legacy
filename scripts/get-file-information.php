<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["identity"]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$file_information = $database->querySingle("SELECT id, location, name, size, type, specialContextMenu " .
	"FROM files JOIN users " .
	"ON files.uID = users.uID LEFT JOIN filetype_associations ON filetype_associations.mime = " .
	"files.type WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' AND id='" . $database->escapeString($_GET["identity"]) . "' LIMIT 1", true);
if ($file_information === false)
  exit ("");
echo "{";
echo "  id: '" . $file_information["id"] . "',\n";
echo "  location: '" . $file_information["location"] . "',\n";
//$file_name = mb_substr($file_information["name"], 0, mb_strlen($file_information["name"]) 
//- mb_strlen(mb_strrchr($file_information["name"], ".")));
//$file_name = preg_replace("/_/", " ", $file_name);
echo "  name: '" . addslashes($file_information["name"]) . "',\n";
echo "  size: '" . $file_information["size"] . "',\n";
echo "  type: '" . addslashes($file_information["type"]) . "',\n";
echo "  contextMenu: [\n";
$basic_file_operations_menu = explode(",", $database->querySingle("SELECT items FROM context_menus WHERE " . 
	"menu_id='1' LIMIT 1"));
foreach ($basic_file_operations_menu as $index => $item_id)
{
  echo "{\n";
  $item = $database->querySingle("SELECT icon, text, action, activationKeyCode FROM context_menu_items WHERE item_id='" .
	$item_id . "' LIMIT 1", true);
  echo "  icon: '" . $item["icon"] . "',\n";
  echo "  text: '" . $item["text"] . "',\n";
  echo "  action: '" . $item["action"] . "'" . ($item["activationKeyCode"] != "" ? "," : "") . "\n";
  if ($item["activationKeyCode"] != "")
    echo "  activationKeyCode: '" . $item["activationKeyCode"] . "'\n";
  echo "}" . ($index < sizeof($basic_file_operations_menu) - 1 ? "," : "") . "\n";
}
echo "  ]\n";
echo "}";
?>
