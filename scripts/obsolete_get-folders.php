<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["location"]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
echo "{";
$folders = $database->query("SELECT ref FROM folders JOIN users ON folders.uID = " .
	"users.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND location='" .
	$database->escapeString($_GET["location"]) . "'");
if ($folders->numberRows() > 0)
{
  echo "folders: [\n";
  $folders_out = 0;
  while ($folder = $folders->fetchArray())
  {
    echo "'" . $folder["ref"] . "'" . ($folders_out < $folders->numberRows() - 1 ? "," : "") . "\n";
    $folders_out++;
  }
  echo "]\n";
}
echo "}";
?>
