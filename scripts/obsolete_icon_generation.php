<?
session_start();
require_once("../config.php");
$conn = mysql_connect(SQL_HOST, SQL_USER, SQL_PASS);
mysql_select_db(SQL_DATABASE, $conn);

$getThemes = mysql_query("SELECT ID, handle, name FROM themes ORDER BY ID ASC");

echo "<table>";
while ($theme = mysql_fetch_array($getThemes, MYSQL_ASSOC))
{
echo "<tr>";
echo "<td>{$theme['handle']} ({$theme['ID']})</td>";
generateThemeIcon($theme['ID']);
echo "<td>";
echo "<img src='" . URL_ROOT . "/themes/icons/theme" . $theme['ID'] . ".jpg'>";
echo "</td>";
echo "</tr>";
}



// generate the icon
function generateThemeIcon($id)
{
$getTheme = mysql_query("SELECT ID, handle, name, iconX, iconY FROM themes WHERE ID='" . $id . "' LIMIT 1");
if (mysql_num_rows($getTheme) == 0)
{
echo "<font color='red'>Error Generating Theme Icon -- No returned theme information for ID '" . $id . "'</font>";
return;
}
$themeData = mysql_fetch_row($getTheme);

$themeIcon = imagecreatetruecolor(THEME_ICON_SIZE, THEME_ICON_SIZE);
$themeBG = imagecreatefromjpeg(ABSOLUTE_PATH . "/themes/backgrounds/" . $themeData[1] . ".jpg");

imagecopyresized($themeIcon, $themeBG, 0, 0, $themeData['3'], $themeData['4'], THEME_ICON_SIZE, THEME_ICON_SIZE, 
  THEME_ICON_SIZE * 2, THEME_ICON_SIZE * 2);

imagejpeg($themeIcon, ABSOLUTE_PATH . "/themes/icons/theme" . $themeData[0] . ".jpg", 100);
}