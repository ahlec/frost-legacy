<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You are not logged in.");
if (!isset($_GET["identity"]) && !isset($_GET["handle"]))
  exit("Parameters were not passed.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$location_information = $database->querySingle("SELECT location_id, locations.uID, gID, handle, name, parent_location, type " . 
	"FROM locations LEFT JOIN " .
	"users ON users.uID = locations.uID WHERE (locations.uID = '0' OR uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "') AND " . (isset($_GET["identity"]) ? "gID='" .
	$database->escapeString($_GET["identity"]) . "' " : "") . (isset($_GET["identity"]) && isset($_GET["handle"]) ? "AND " : "") .
	(isset($_GET["handle"]) ? "handle='" . $database->escapeString($_GET["handle"]) . "' " : "") . "LIMIT 1", true);	

if ($location_information === false)
  exit ("Unable to load location information.");

echo "{";
echo "  id: '" . $location_information["location_id"] . "',\n";
echo "  gID: '" . $location_information["gID"] . "',\n";
echo "  handle: '" . $location_information["handle"] . "',\n";
echo "  name: '" . mb_addslashes($location_information["name"]) . "',\n";
echo "  contextHandles: ['location','" . ($location_information["type"] == "Special" && $location_information["handle"] != "" ?
	addslashes($location_information["handle"]) : strtolower($location_information["type"])) . "'],\n";
echo "  type: '" . $location_information["type"] . "',\n";

echo "  heritage: [\n";
$current_location = $location_information["parent_location"];
while ($current_location != "0")
{
  $current_location_info = $database->querySingle("SELECT location_id, handle, name, parent_location FROM locations LEFT JOIN users " .
	"ON users.uID = locations.uID WHERE (locations.uID = '0' OR uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"') AND location_id='" . $current_location . "' LIMIT 1", true);
  echo "    {\n";
  echo "      location_id:'" . $current_location_info["location_id"] . "',\n";
  echo "      name: '" . $current_location_info["name"] . "'\n";
  echo "    }" . ($current_location_info["parent_location"] != "0" ? "," : "") . "\n";
  $current_location = $current_location_info["parent_location"];
}
echo "    ],\n";

echo "  contents: [\n";
$contents_locations = $database->query("SELECT location_id, gID, handle, name, type, custom_icon, program, dateLastRelocated " .
	" FROM locations LEFT JOIN users ON users.uID = locations.uID LEFT JOIN filetype_associations ON " .
	"filetype_associations.mime = LOWER(IFNULL(handle, " .
	"type)) WHERE (locations.uID = '0' OR uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .	"') AND parent_location='" . 
	$location_information["location_id"] . "'");
$contents_files = $database->query("SELECT gID, name, size, files.mime, icon, icon_generated, program, dateLastRelocated " .
	"FROM files JOIN users ON users.uID = files.uID " .
	"LEFT JOIN filetype_associations ON files.mime = filetype_associations.mime WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND location='" . $location_information["gID"] . "'");
$locations_index = 0;
while ($contents_location = $contents_locations->fetchArray())
{
  echo "      {\n";
  echo "        type:'location',\n";
  echo "        icon:'";
  if ($contents_location["custom_icon"] != "")
    echo $contents_location["custom_icon"];
  else if ($contents_location["type"] == "Special")
  {
    if ($contents_location["handle"] == "trash")
      echo "trash-empty.png";
  } else if ($contents_location["type"] == "Folder")
    echo "folder.png";
  echo "',\n";
  echo "        location_id:'" . $contents_location["location_id"] . "',\n";
  echo "        gID:'" . $contents_location["gID"] . "',\n";
  echo "        handle:'" . $contents_location["handle"] . "',\n";
  echo "        name:'" . mb_addslashes($contents_location["name"]) . "',\n";
  echo "        lastRelocated:'" . $contents_location["dateLastRelocated"] . "',\n";
  if ($location_information["gID"] == "2")
    echo "        contextHandles:['trashItem']\n";
  else
  {
    echo "        defaultProgram:'" . $contents_location["program"] . "',\n";
    echo "        contextHandles:['location'" . ($contents_location["type"] == "Special" && $contents_location["handle"] != "" ?
	",'" . addslashes($contents_location["handle"]) . "'" : ",'" . strtolower($contents_location["type"]) . "'") . "]\n";
  }
  echo "      }" . ($locations_index < $contents_locations->numberRows() - 1 || $contents_files->numberRows() > 0 ? "," : "") . "\n";
  $locations_index++;
}
$files_index = 0;
while ($contents_file = $contents_files->fetchArray())
{
  echo "      {\n";
  echo "        type:'file',\n";
  echo "        icon:'" . ($contents_file["icon_generated"] == 1 ? "get.php?gid=" . $contents_file["gID"] :
	$contents_file["icon"]) . "',\n";
/*  echo "        icon:'" . $contents_file["icon"] . ($contents_file["icon_generation"] == "1" ? "?gid=" . $contents_file["gID"] : 
"") .  "',\n";*/
  echo "        gID:'" . $contents_file["gID"] . "',\n";
  echo "        name:'" . mb_addslashes($contents_file["name"]) . "',\n";
  echo "        size:'" . $contents_file["size"] . "',\n";
  echo "        mime:'" . $contents_file["mime"] . "',\n";
  echo "        lastRelocated:'" . $contents_file["dateLastRelocated"] . "',\n";
  if ($location_information["gID"] == "2")
    echo "        contextHandles:['trashItem']\n";
  else
  {
    echo "        defaultProgram:'" . $contents_file["program"] . "',\n";
    echo "        contextHandles:['file','" . $contents_file["type"] . "']\n";
  }
  echo "      }" . ($files_index < $contents_files->numberRows() - 1 ? "," : "") . "\n";
  $files_index++;
}
echo "    ]\n";

echo "}";
?>
