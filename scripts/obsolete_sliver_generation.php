<?
session_start();
require_once("../config.php");
$conn = mysql_connect(SQL_HOST, SQL_USER, SQL_PASS);
mysql_select_db(SQL_DATABASE, $conn);

$getThemes = mysql_query("SELECT ID, handle, name FROM themes ORDER BY ID ASC");

echo "<table>";
while ($theme = mysql_fetch_array($getThemes, MYSQL_ASSOC))
{
echo "<tr>";
echo "<td>{$theme['handle']} ({$theme['ID']})</td>";
echo "<td>";
echo (generateThemeSlivers($theme['ID']) ? "<font color='green'>Successful</font>" : "<font color='red'>Unsuccessful</font>");
echo "</td>";
echo "</tr>";
}
echo "</table>";


// generate the icon
function generateThemeSlivers($id)
{
$getTheme = mysql_query("SELECT ID, handle FROM themes WHERE ID='" . $id . "' LIMIT 1");
if (mysql_num_rows($getTheme) == 0) { return false; }
$themeData = mysql_fetch_row($getTheme);

$themeBG = imagecreatefromjpeg(ABSOLUTE_PATH . "/themes/backgrounds/" . $themeData[1] . ".jpg");
$sliverVertical = imagecreatetruecolor(1, imagesy($themeBG));

imagecopyresized($sliverVertical, $themeBG, 0, 0, imagesx($themeBG) - 1, 0, 1, imagesy($themeBG), 1, imagesy($themeBG));

return imagejpeg($sliverVertical, ABSOLUTE_PATH . "/themes/slivers/vertical" . $themeData[0] . ".jpg", 100);
}