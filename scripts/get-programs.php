<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$programs = $database->query("SELECT program_id, handle, title, file, icon, isApplication FROM programs"); 
if ($programs === false)
  exit ("");
echo "[";
$programs_out = 0;
while ($program = $programs->fetchArray())
{
  echo "  {\n";
  echo "     id: '" . $program["program_id"] . "',\n";
  echo "     handle: '" . $program["handle"] . "',\n";
  echo "     title: '" . addslashes($program["title"]) . "',\n";
  echo "     file: '" . $program["file"] . "',\n";
  echo "     isApplication: '" . ($program["isApplication"] == "1" ? "true" : "false") . "',\n";
  echo "     icon: '" . $program["icon"] . "'}";
  if ($programs_out < $programs->numberRows() - 1)
    echo ",";
  echo "\n";
  $programs_out++;
}
echo "]";
?>
