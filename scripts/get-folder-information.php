<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["identity"]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
$folder_information = $database->querySingle("SELECT foldername, ref, canModify, icon, location, location_id FROM folders JOIN 
users " .
	"ON folders.uID = users.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"' AND ref='" . $database->escapeString($_GET["identity"]) . "' LIMIT 1", true);
if ($folder_information === false)
  exit ("");
echo "{";
echo "  name: '" . $folder_information["foldername"] . "',\n";
echo "  icon: '" . $folder_information["icon"] . "',\n";
echo "  id: '" . $folder_information["ref"] . "',\n";
echo "  location: [\n";
$current_location = $folder_information["location_id"];
do
{
  $current_location_info = $database->querySingle("SELECT location_id, handle, name, parent_location FROM locations LEFT JOIN users " .
	"ON users.uID = locations.uID WHERE (locations.uID = '0' OR uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) .
	"') AND location_id='" . $current_location . "' LIMIT 1", true);
  echo "    {\n";
  echo "      location_id:'" . $current_location_info["location_id"] . "',\n";
  echo "      name: '" . $current_location_info["name"] . "'\n";
  echo "    }" . ($current_location_info["parent_location"] != "0" ? "," : "") . "\n";
  $current_location = $current_location_info["parent_location"];
} while ($current_location != "0");
echo "    ],\n";
echo "  canModify: '" . ($folder_information["canModify"] == "1" ? "true" : "false") . "'\n";
echo "}";
?>
