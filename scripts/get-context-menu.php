<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You are not logged in.");

require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);

$items = $database->query("SELECT icon, text, action, conditional, context_handles, " .
	"weight, activationKeyCode, submenu FROM context_menu_items");
if ($context_items === false)
  exit ("Error when attempting to load context menu");

echo "[\n";
$index = 0;
while ($item = $items->fetchArray())
{
  echo "  {\n";
  echo "    icon: '" . $item["icon"] . "',\n";
  echo "    text: '" . $item["text"] . "',\n";
  echo "    conditional: '" . $item["conditional"] . "',\n";
  echo "    weight: '" . $item["weight"] . "',\n";
  echo "    contextHandles: [";
  $context_handles = explode(",", $item["context_handles"]);
  foreach ($context_handles as $index => $handle)
    echo "'" . $handle . "'" . ($index < sizeof($context_handles) - 1 ? "," : "");
  echo "],\n";
  echo "    action: '" . $item["action"] . "'" .
	($item["activationKeyCode"] != "" || $item["submenu"] != "" ? "," : "") . "\n";
  if ($item["activationKeyCode"] != "")
    echo "    activationKeyCode: '" . $item["activationKeyCode"] . "'" .
		($item["submenu"] != "" ? "," : "") . "\n";
  if ($item["submenu"] != "")
	echo "    submenu: " . $item["submenu"] . "\n";
  echo "  }" . ($index < $items->numberRows() - 1 ? "," : "") . "\n";
  $index++;
}
echo "]\n";
?>
