<?php
require_once ("../framework/config.php");
@session_start();
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_GET["theme"]))
  exit ("");
$theme_information = $database->querySingle("SELECT ID, handle, name, stylesheet, background_image, " .
	"icon_file FROM themes WHERE ID='" .
	$database->escapeString($_GET["theme"]) . "' LIMIT 1", true);
echo "{";
echo " name: '" . $theme_information["name"] . "',\n";
echo " handle: '" . $theme_information["handle"] . "',\n";
echo " id: '" . $theme_information["ID"] . "',\n";
echo " stylesheet: '" . $theme_information["stylesheet"] . "',\n";
echo " background: '" . $theme_information["background_image"] . "',\n";
echo " icon: '" . $theme_information["icon_file"] . "'\n";
echo "}";
?>
