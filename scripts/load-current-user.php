<?php
require_once ("../framework/config.php");
@session_start();
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
$user_information = $database->querySingle("SELECT uID, username, theme, email, isAdmin FROM users WHERE uHash='" .
	$database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' LIMIT 1", true);
echo "{";
echo " username: '" . $user_information["username"] . "',\n";
echo " id: '" . $user_information["uID"] . "',\n";
echo " theme: '" . $user_information["theme"] . "',\n";
echo " emailHash: '" . md5($user_information["email"]) . "',\n";
echo " admin: '" . ($user_information["isAdmin"] == "1" ? "true" : "false") . "'\n";
echo "}";
?>
