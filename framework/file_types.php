<?
function GetMIMEIcon($filetype){
if($filetype == ".txt") { return "icon-text.png"; }
if($filetype == ".jpg" || $filetype == ".bmp" || $filetype == ".png") { return "icon-picture.png"; }
if($filetype == ".php") { return "icon-php.png"; }
if($filetype == ".doc" || $filetype == ".docx") { return "icon-word.png"; }
if($filetype == ".ppt" || $filetype == ".pptx" ) { return "icon-powerpoint.png"; }
if($filetype == ".pdf") { return "icon-pdf.png"; }
if($filetype == ".xls" || $filetype == ".xlsx") { return "icon-excel.png"; }
if($filetype == ".html" || $filetype == ".htm") { return "icon-web.png"; }
if($filetype == ".zip") { return "icon-zip.png"; }
if($filetype == ".flv" || $filetype == ".swf") { return "icon-flash.png"; }
return "icon-nofile.png"; //default
}
function CanOpenMIME($filetype){
if($filetype == ".jpg" || $filetype == ".bmp" || $filetype == ".png") { return true; }
if($filetype == ".txt") { return true; }
return false;
}
function OpenMIMEFunction($filetype){
if($filetype == ".jpg" || $filetype == ".bmp" || $filetype == ".png") { return "image"; }
if($filetype == ".txt") { return "text"; }
return "";
}
?>
