<?
require_once ("/home/deitloff/global.php");
ini_set("session.cookie_domain", "deitloff.com");

define("URL_ROOT", "http://www.frost.deitloff.com");
define("WEB_PATH", URL_ROOT);
define("ABSOLUTE_PATH", "/home/deitloff/public_html/filesystem");
define("DOCUMENT_ROOT", "/home/deitloff/public_html/filesystem");
define("IMPEC_PATH", "/home/deitloff/filesystem");
define("IMAGICK_PATH", "/usr/bin/convert");

header("Expires: Sun, 19 Nov 1978 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once (DOCUMENT_ROOT . "/framework/multibyte-functions.php");
?>
