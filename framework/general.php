//document.onclick = processGenericClick;
document.onmousemove = trackMousePos;
//document.oncontextmenu = new Function("return false");

var MouseX = 0;
var MouseY = 0;
function trackMousePos(e){
if (document.all) {
    MouseX = event.clientX + document.body.scrollLeft;
    MouseY = event.clientY + document.body.scrollTop;
  } else {
    MouseX = e.pageX;
    MouseY = e.pageY;
    }
}
function processGenericClick(e){
if(ConsoleDisabled) { return false; }
if(window.event) { var e = window.event; }
var clickID = e.srcElement? e.srcElement.id : e.target.id;
if(clickID != "TaskBarUser") { CloseUserMenu(); }
var rightclick = false;
if (e.which) rightclick = (e.which > 1); else if (e.button) rightclick = (e.button > 1);
if(rightclick) {
var ContextMenu = document.body.appendChild(document.createElement('div'));
ContextMenu.setAttribute('id', 'ContextMenu');
document.getElementById('ContextMenu').className = "ContextMenu";
if((document.body.clientHeight / 2) < MouseY){
document.getElementById('ContextMenu').style.bottom = document.body.clientHeight - MouseY;
}else{ document.getElementById('ContextMenu').style.top = MouseY; }
document.getElementById('ContextMenu').style.left = MouseX;
}else{
if(document.getElementById('ContextMenu')){
document.body.removeChild(document.getElementById('ContextMenu'));}
}
}

var ConsoleDisabled = false;
function ToggleConsole(enable)
{
ConsoleDisabled = !enable;
//fade("DisableBackground");
if(enable)
{
if(document.getElementById('DisableBackground')){
document.body.removeChild(document.getElementById('DisableBackground')); }
}else{
var disableBackground = document.body.appendChild(document.createElement('div'));
disableBackground.setAttribute('id', "DisableBackground");
disableBackground.className = "DisabledConsole";
disableBackground.style.zIndex = OpenWindows.length + 1;
}
}

function ajaxRequest(Url,DivId)
{
 var AJAX;
 try
 {
  AJAX = new XMLHttpRequest();
 }
 catch(e)
 {
  try
  {
   AJAX = new ActiveXObject("Msxml2.XMLHTTP");
  }
  catch(e)
  {
   try
   {
    AJAX = new ActiveXObject("Microsoft.XMLHTTP");
   }
   catch(e)
   {
    alert("Your browser does not support AJAX.");
    return false;
   }
  }
 }
 AJAX.onreadystatechange = function()
 {
  if(AJAX.readyState == 4)
  {
   if(AJAX.status == 200)
   {
if(DivId=="LoginTitle")
{
if(AJAX.responseText=="1"){
LoginComplete();
//window.location.href = "index.php";
}else{
document.LoginForm.UsernameLogin.value="";
document.LoginForm.PasswordLogin.value="";
}
}else{
    document.getElementById(DivId).innerHTML = AJAX.responseText;
}
   }
   else
   {
    alert("Error: "+ AJAX.statusText +" "+ AJAX.status);
   }
  }
 }
 AJAX.open("get", Url, true);
 AJAX.send(null);
}

function CreateInterface()
{
document.title = 'Archives'; //progSettings[2];

//Desktop
var Desktop = document.body.appendChild(document.createElement('div'));
Desktop.setAttribute('id', 'Desktop');
Desktop.className = 'Desktop';
Desktop.onclick = resetDesktop;
ajaxRequest("screens/desktop.php?scrWdth=" + document.body.clientWidth,"Desktop");

var TaskBar = document.body.appendChild(document.createElement('div'));
var TaskBarOverlay = TaskBar.appendChild(document.createElement('div'));
TaskBarOverlay.className = "TileOverlay";
TaskBarOverlay.setAttribute("id", "TaskBarOverlay");
var TaskBarTbl = TaskBarOverlay.appendChild(document.createElement('table'));
var TaskBarUser = TaskBarOverlay.appendChild(document.createElement('div'));

//document.body.appendChild(TaskBar);
//TaskBar.appendChild(TaskBarTbl);

TaskBar.setAttribute('id', 'TaskBar');
TaskBarTbl.setAttribute('id', 'TaskBarTable');
TaskBarTbl.onclick = FocusWindowFromTaskBar;

document.getElementById('TaskBar').className = 'TaskBar';
document.getElementById('TaskBarTable').className = 'TaskBarTable';

var taskBarRow = TaskBarTbl.insertRow(0);
taskBarRow.setAttribute('id', 'TaskBarRow');

TaskBarUser.setAttribute('id', 'TaskBarUser');
TaskBarUser.className = "TaskBarUser";
TaskBarUser.innerHTML = userName + " <img src='images/icon-user.png'>";
TaskBarUser.onclick = ToggleUserBarMenu;

setupUserMenu();

setupActionsBar();
SessionLoaded();
}

function refreshDesktop()
{
ajaxRequest("screens/desktop.php?scrWdth=" + document.body.clientWidth,"Desktop");
}

function LoginInterface(){
ToggleConsole(false);
document.title = "Login";
var BackImg = "images/bg-tiled.jpg";
document.body.background = BackImg;
document.body.style.backgroundRepeat = "repeat";
CreateWindow("Login",300,110,"Center",null,"Login","login.php", false, false);
document.getElementById("Login").style.zIndex = "2";
document.getElementById('LoginTitle').removeChild(document.getElementById('CloseLogin'));
document.getElementById('LoginTitle').style.cursor = "default";
var themeStyleSheet = document.createElement('link');
themeStyleSheet.href = 'themes/theme.php?theme=0';
themeStyleSheet.rel = "stylesheet";
themeStyleSheet.type = "text/css";
themeStyleSheet.setAttribute('id', 'ThemeStyleSheet');
document.getElementsByTagName("head")[0].appendChild(themeStyleSheet);
}

function ProcessLogin(){
var UsernameInput = document.LoginForm.UsernameLogin.value;
var PasswordInput = document.LoginForm.PasswordLogin.value;
var PageRequest = "login.php?username=" + UsernameInput + "&password=" + PasswordInput;
ajaxRequest(PageRequest, "LoginTitle");
}

function LoginValidate(e){
if(window.event) { var e = window.event; }
var subBtn = document.getElementById("LoginSubmitButton");
var keycode = e.keyCode;
if(keycode == 13 && !subBtn.disabled) { ProcessLogin(); }

var invalidInput = false;
var usrInput = document.getElementById("UsernameInput");
var pwdInput = document.getElementById("PasswordInput");
var usri = usrInput.value;
var pwdi = pwdInput.value;
if(stringContainsInvalidChars(usri)) {
usrInput.className = "InvalidInputBox";
invalidInput = true;
}else{
usrInput.className = "ValidInputBox";
}
if(stringContainsInvalidChars(pwdi)) {
pwdInput.className = "InvalidInputBox";
invalidInput = true;
}else{
pwdInput.className = "ValidInputBox";
}

if(usri !="" && pwdi !="" && !invalidInput){
subBtn.disabled=false;
}else{
subBtn.disabled=true;
}

}

function LoginComplete()
{
if(!ConsoleDisabled) { ToggleConsole(false); }
if(document.getElementById('Login') != null) { DeleteWindow("Login"); }
document.body.style.backgroundRepeat = "no-repeat";
CreateWindow("LoadingSessionWindow",300,100,"Center",null,"Loading Session","loading.php", false, false);
document.getElementById('LoadingSessionWindowTitle').removeChild(document.getElementById('CloseLoadingSessionWindow'));
document.getElementById('LoadingSessionWindowTitle').style.cursor = "default";
var uvScript = document.createElement('script');
uvScript.src = "javascripts/user_variables.php";
uvScript.type = "text/javascript";
document.getElementsByTagName("head")[0].appendChild(uvScript);

if(!document.getElementById('ThemeStyleSheet'))
{
var themeStyleSheet = document.createElement('link');
themeStyleSheet.rel = "stylesheet";
themeStyleSheet.type = "text/css";
themeStyleSheet.setAttribute('id', 'ThemeStyleSheet');
document.getElementsByTagName("head")[0].appendChild(themeStyleSheet);
}
document.getElementById('ThemeStyleSheet').href = 'themes/theme.php?theme=' + userTheme;

LoadSessionBackground();
}

function SetTheme(ThemeHandle, ThemeID)
{
ToggleConsole(false);
CreateWindow("ChangeThemeWindow",300,100,"Center",null,"Changing Theme","loading.php", false, false);
document.getElementById('ChangeThemeWindowTitle').removeChild(document.getElementById('CloseChangeThemeWindow'));
document.getElementById('ChangeThemeWindowTitle').style.cursor = "default";
var backgroundImg = document.body.appendChild(document.createElement('img'));
backgroundImg.setAttribute("id", "BackgroundImage");
backgroundImg.style.display = "none";
backgroundImg.src = 'themes/backgrounds/' + ThemeHandle + '.jpg';
backgroundImg.ThemeID = ThemeID;
backgroundImg.onload = SetThemeBGLoaded;
}

function SetThemeBGLoaded()
{
var ThemeID = document.getElementById("BackgroundImage").ThemeID;
document.body.background = document.getElementById("BackgroundImage").src;
document.body.removeChild(document.getElementById("BackgroundImage"));
document.getElementById('ThemeStyleSheet').href = "themes/theme.php?theme=" + ThemeID;
DeleteWindow("ChangeThemeWindow");
ToggleConsole(true);
}

function LoadSessionBackground()
{
var backgroundImg = document.body.appendChild(document.createElement('img'));
backgroundImg.setAttribute("id", "BackgroundImage");
backgroundImg.src = 'themes/backgrounds/' + userThemeHandle + '.jpg'; //progSettings[0];
backgroundImg.onload = SessionBackgroundLoaded;
}

function SessionBackgroundLoaded()
{
document.body.removeChild(document.getElementById("BackgroundImage"));
document.body.background = 'themes/backgrounds/' + userThemeHandle + '.jpg'; //progSettings[0];
//document.body.style.backgroundRepeat = progSettings[1];
//document.body.style.backgroundRepeat = 'repeat-x';
CreateInterface();
}

function SessionLoaded()
{
DeleteWindow("LoadingSessionWindow");
ToggleConsole(true);
}

function postAJAX(url,DivId,parameters)
{
 var AJAX;
 try
 {
  AJAX = new XMLHttpRequest();
 }
 catch(e)
 {
  try
  {
   AJAX = new ActiveXObject("Msxml2.XMLHTTP");
  }
  catch(e)
  {
   try
   {
    AJAX = new ActiveXObject("Microsoft.XMLHTTP");
   }
   catch(e)
   {
    alert("Your browser does not support AJAX.");
    return false;
   }
  }
 }

 AJAX.onreadystatechange = function()
 {
  if(AJAX.readyState == 4)
  {
   if(AJAX.status == 200) {
    document.getElementById(DivId).innerHTML = AJAX.responseText;
   } else { alert("Error: "+ AJAX.statusText +" "+ AJAX.status); }
  }
 }

AJAX.open('POST', url, true);
AJAX.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
AJAX.setRequestHeader("Content-length", parameters.length);
AJAX.setRequestHeader("Connection", "close");
AJAX.send(parameters);
}

function uploadFileAJAX()
{
var fileInput = document.getElementById("UploadFileInput");
var poststr = "file=" + encodeURI( fileInput.value );
alert(poststr);
postAJAX('processes/upload_file.php', 'UploadDialogBody', poststr);
}

function openTrashBin(){
CreateWindow('TrashFolder',450,350,"Default",'icon-trash.png','Trash Bin', 'trash.php', true, true);
document.getElementById('TrashFolderBody').style.paddingLeft = "0";
actionsBarTrash();
}

function CreateFolderValidate(e)
{
var btnInput = document.getElementById("CreateFolderInput");
var cfi = document.getElementById("CreateFolderInput").value;
var invalid = stringContainsInvalidChars(cfi);
if(invalid)
{
btnInput.className = "InvalidInputBox";
}else{
btnInput.className = "ValidInputBox";
}
var confButton = document.getElementById('CreateFolderButton');
if(cfi != "" && !invalid)
{
confButton.value = "Create Folder";
confButton.onclick = confirmFolderCreate;
}else{
confButton.value = "Cancel";
confButton.onclick = cancelFolderCreate;
}
}

function trashUnselect(e)
{
FocusWindow('TrashFolder');
if(window.event) { var e = window.event; }
var srcEle = e.srcElement? e.srcElement : e.target;
if(srcEle.id != "TrashBody") { return; }
actionsBarTrash();
}

function trashSelectItem(fileID)
{
actionsBarTrashRestore(fileID);
}

function EmptyTrash(){
ajaxRequest("processes/empty_trash.php", 'TrashFolderBody');
}

function RestoreTrashItem(e)
{
var fileID = document.getElementById('ABI_RestoreTrash').fileID;
if(!fileID) { return; }
var TrashCall = "processes/restore_file.php?file=" + fileID;
ajaxRequest(TrashCall, 'TrashBody');
RefreshWindow("Folder_0");
}

function resetDesktop(e)
{
if(window.event) { var e = window.event; }
var srcEle = e.srcElement? e.srcElement : e.target;
if(srcEle.id != "Desktop") { return; }
actionsBarDesktop();
}

function stringContainsInvalidChars(str)
{
var invalidChars = new Array('.','?',';','"',"'",',','#','$','%','^','&','*','!','@','`','~');
for(var search = 0; search < invalidChars.length; search++)
{
if(str.indexOf(invalidChars[search]) > -1) { return true; }
}
return false;
}

function validateUploadFile()
{
var fileInput = document.getElementById("UploadFileInput");
var fileButton = document.getElementById('UploadFileButton');
if(fileInput.value != "")
{
fileButton.setAttribute('type', 'submit');
fileButton.value = "Upload";
fileButton.onclick = null;
}else{
fileButton.setAttribute('type', 'button');
fileButton.value = "Cancel";
fileButton.onclick = finishUploadFile;
}
}
