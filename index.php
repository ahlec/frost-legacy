<?
require_once ("/home/deitloff/www/filesystem/framework/config.php");
require_once (DOCUMENT_ROOT . "/framework/database.php");
$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME,
	FILESYSTEM_PASSWORD, FILESYSTEM_DATABASE);
	
ini_set("session.cookie_domain", "deitloff.com");
@session_start();

echo "<!DOCTYPE html>\n";
echo "<html xlmns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
echo "  <head>\n";
echo "    <title>frOSt</title>\n";
echo "    <script> var web_path = '" . URL_ROOT . "'; </script>\n";
$core_files = $database->query("SELECT file_id, type, filepath FROM core_files WHERE active='1'");
while ($core_file = $core_files->fetchArray())
{
  if ($core_file["type"] == "css")
    echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"" . WEB_PATH . $core_file["filepath"] . "\" id=\"core-file-" .
    	$core_file["file_id"] . "\" />\n";
  else if ($core_file["type"] == "javascript")
    echo "    <script type=\"text/javascript\" src=\"" . WEB_PATH . $core_file["filepath"] . "\" id=\"core-file-" .
    	$core_file["file_id"] . "\"></script>\n";
}
echo "    <link rel=\"stylesheet\" type=\"text/css\" id=\"theme-stylesheet\">\n";
echo "  </head>\n";

echo "<body onLoad=\"Browser.initialize();setupContextMenu();Theme.setup();setupDesktop();\">\n";
?>
