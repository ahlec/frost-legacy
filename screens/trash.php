<?php
require_once ("../framework/config.php");
require_once ("../framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You are not currently logged in.");

$number_files_in_trash = $database->querySingle("SELECT count(*) FROM files JOIN users ON files.uID = " .
	"users.uID WHERE uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND " .
	"location='trash'");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'absolute',\n";
  echo "  top:'10px',\n";
  echo "  left:'10px',\n";
  echo "  width:'450px',\n";
  echo "  height:'400px',\n";
  echo "  canClose:'true',\n";
  echo "  title:'Trash',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  icon:'" . $folder_information["icon"] . "'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<pre>\n";
echo $number_files_in_trash;
echo "</pre>\n";

?>
