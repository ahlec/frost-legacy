<?php
require_once ("../framework/config.php");
require_once ("../framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]) || !isset($_GET["parameter"]) ||
	$database->querySingle("SELECT count(*) FROM folders JOIN users ON folders.uID = users.uID WHERE " .
	"uHash='" . $database->escapeString($_SESSION[DEITLOFF_SESSION]) . "' AND ref='" .
	$database->escapeString($_GET["parameter"]) . "'") == 0)
{
  exit ("This folder does not exist, or you are not the owner of it.");
}

$folder_information = $database->querySingle("SELECT foldername, ref, icon FROM folders WHERE ref='" .
	$database->escapeString($_GET["parameter"]) . "' LIMIT 1", true);

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'absolute',\n";
  echo "  top:'10px',\n";
  echo "  left:'10px',\n";
  echo "  width:'550px',\n";
  echo "  height:'500px',\n";
  echo "  sidebar:'left',\n";
  echo "  canClose:'true',\n";
  echo "  title:'" . $folder_information["foldername"] . "',\n";
  echo "  onTaskbar:'true',\n";
  echo "  canMove:'true',\n";
  echo "  icon:'" . $folder_information["icon"] . "'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<pre>\n";
print_r($folder_information);
echo "</pre>\n";
echo "hiiiii";

?>
