<?php
if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  width:'375px',\n";
  echo "  height:'100px',\n";
  echo "  modal:'true',\n";
  echo "  className:'loginPanel',\n";
  echo "  onTaskbar:'false'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<style>
div.loginPanel
{
  font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;
  background-color:white;
  border:1px solid #2B2320;
  padding:3px;
  text-align:center;
  padding-top:10px;
}
div.loginPanel div.logo
{
  position:absolute;
  top:-100px;
  right:6px;
  width:128px;
  height:128px;
  background:url('images/interface/128logo.png') no-repeat top left;
}
div.loginPanel div.label
{
  text-align:left;
  font-size:13px;
  font-weight:bold;
  margin-left:10px;
}
div.loginPanel input.textbox
{
  font-size:11px;
  background-color:#636363;
  border:1px solid black;
  color:black;
  width:200px;
  text-align:center;
  margin-bottom:5px;
}
div.loginPanel input.button
{
  display:block;
  margin-left:auto;
  margin-right:auto;
  border:1px solid black;
  background-color:#8A8A8A;
}
</style>\n";

echo "<script>
  checkLoginFormComplete = function(e)
	{
	  if (!e)
	    var e = window.event;
	  document.getElementById('loginButton').disabled = 
		(document.getElementById('loginUsername').value.length == 0 ||
		document.getElementById('loginUsername').value.match(/[^0-9a-zA-Z ]/) || 
		document.getElementById('loginPassword').value.length == 0);
	  if ((e.which || e.keyCode) == 13 && !document.getElementById('loginButton').disabled)
		document.getElementById('loginButton').click();
	};
  processLogin = function()
	{
	  if (document.getElementById('loginButton').disabled)
	    return;
	  document.getElementById('loginUsername').disabled = true;
	  document.getElementById('loginPassword').disabled = true;
	  document.getElementById('loginButton').disabled = true;
	  var loginCall = \"scripts/login.php?username=\" +
		document.getElementById('loginUsername').value +
		\"&password=\" + 
hex_md5(document.getElementById('loginPassword').value);
	  executeAJAX(loginCall, function process(results)
	  {
	    if (results == 'success')
	    {
	      setupDesktop();
	      return;
	    }
	    alert(results.replace('[Error] ', ''));
	    document.getElementById('loginUsername').disabled = false;
	    document.getElementById('loginPassword').disabled = false;
	    document.getElementById('loginPassword').value = '';
	    document.getElementById('loginPassword').focus();
	    document.getElementById('loginPassword').select();
	  });
	};
</script>\n";

echo "<div class=\"logo\"></div>\n";
echo "<div class=\"label\">Username</div>\n";
echo "<input class=\"textbox\" type=\"text\" id=\"loginUsername\" onKeyUp=\"checkLoginFormComplete(event);\" " .
	"autofocus=\"autofocus\"/>\n";
echo "<div class=\"label\">Password</div>\n";
echo "<input class=\"textbox\" type=\"password\" id=\"loginPassword\" onKeyUp=\"checkLoginFormComplete(event);\" />\n";
echo "<input class=\"button\" type=\"button\" id=\"loginButton\" disabled=\"disabled\" value=\"Login\" " .
	"onClick=\"processLogin();\" />";
?>
