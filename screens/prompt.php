<?php
require_once ("../framework/config.php");
require_once ("../framework/database.php");
@session_start();

$database = new VersatileDatabase(FILESYSTEM_HOST, FILESYSTEM_USERNAME, FILESYSTEM_PASSWORD,
	FILESYSTEM_DATABASE);
if (!isset($_SESSION[DEITLOFF_SESSION]))
  exit ("You must be logged in.");

if (isset($_GET["design"]) && $_GET["design"] == "JSON")
{
  echo "{frame: {\n";
  echo "  position:'centered',\n";
  echo "  width:'400px',\n";
  echo "  height:'150px',\n";
  echo "  title:'" . $_GET["parameter2"] . "',\n";
  echo "  icon:'rename.php',\n";
  echo "  modal:'true',\n";
  echo "  onTaskbar:'false'\n";
  echo "  }\n";
  echo "}\n";
  exit();
}

echo "<div class=\"promptContainer\">\n";
echo "  <div class=\"question\">" . $_GET["parameter1"] . "</div>\n";
for ($button = 0; $button < $_GET["parameter3"]; $button++)
{
  echo "<input type=\"button\" class=\"button " . $_GET["parameter" .
	(3 + 2 * $button + 2)] . "\" value=\"" . $_GET["parameter" .
	(3 + 2 * $button + 1)] . "\" id=\"promptButton-" . 
	(1 + $button) . "\" />\n";
}
echo "</div>\n";
?>
