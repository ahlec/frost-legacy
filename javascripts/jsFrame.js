function Frame(handle)
{
  if (Frame.getByHandle(handle))
    return Frame.getByHandle(handle);
  this.handle = handle;
  
  var frameInformation = null;

  var paneInformation = { };
  this.paneInformation = paneInformation;

  var frameObject = document.createElement("div");
  frameObject.setAttribute("id","frame-" + handle);
  frameObject.frameClass = this;
  frameObject.base = this;

  var frameContent = document.createElement("div");
  frameContent.className = "frameContent";
  frameObject.appendChild(frameContent);
  
  this.sidebar = null;
  this.tabContainer = null;

  var canMove = false;
  var verticalCentering = false;
  var horizontalCentering = false;
  var isModal = false;
  var onTaskbar = false;
  var layer = null;
  var titleBar = null;

  this.getIcon = function() { return frameInformation.icon; };
  this.getIconSmall = function() { return frameInformation.icon16; };
  this.getFrameContent = function() { return frameContent; };
  this.getHandle = function() { return handle; };

  this.getTitleBarElement = function() { return titleBar; };
  this.getTitle = function() { return titleBar.textElement.innerHTML; };  
  this.setLayer = function(new_layer) {
    layer = (new_layer < 1 ? 1 : new_layer);
    frameObject.style.zIndex = layer;
    frameObject.className = "frameBase" + (layer == Frame.currentTopLayer - 1 ? " focus" : "");
  };
  this.getLayer = function() { return layer; };
  
  this.focus = function() {
    if (isModal || frameObject.className == "frameBase focus")
	return;
    var allOpenFrames = Frame.getAll();
    for (var index = 0; index < allOpenFrames.length; index++)
    {
      if (allOpenFrames[index] == frameObject.frameClass)
	continue;
      allOpenFrames[index].setLayer(allOpenFrames[index].getLayer() - (allOpenFrames[index].getLayer() <=
	layer ? 0 : 1));
    }
    frameObject.frameClass.setLayer(Frame.currentTopLayer - 1);
    if (onTaskbar)
      Taskbar.focus(frameObject.frameClass);
    if (IconItem.itemWithFocus)
      IconItem.itemWithFocus.unfocus();
  };
  frameObject.onmousedown = this.focus;

  this.processNewWindowSize = function() {
     if (verticalCentering)
	frameObject.style.top = ((document.getElementById("background-image").clientHeight -
		(Taskbar.taskbarContainer ? Taskbar.taskbarContainer.offsetHeight : 0) -
		frameObject.offsetHeight) / 2).toString() + "px";
     if (horizontalCentering)
	frameObject.style.left = ((document.getElementByID("background-image").clientWidth -
		frameObject.offsetWidth) / 2).toString() + "px";

     if (frameObject.offsetHeight + frameObject.style.top.replace("px", "") >
	document.getElementById("background-image").clientHeight - (Taskbar.taskbarContainer ?
        Taskbar.taskbarContainer.offsetHeight : 0))
	frameObject.style.top = (document.getElementById("background-image").clientHeight -
		(Taskbar.taskbarContainer ? Taskbar.taskbarContainer.offsetHeight : 0) - frameObject.offsetHeight) + "px";
     if (canMove)
	Drag.init(document.getElementById("title-" + handle), frameObject,
                0, document.getElementById("background-image").clientWidth - frameObject.offsetWidth,
                0, document.getElementById("background-image").clientHeight - frameObject.offsetHeight -
                (Taskbar.taskbarContainer ? Taskbar.taskbarContainer.offsetHeight : 0)); 
  };

  this.load = function(page_url, parameter, loaded_callback, load_contents)
  {
    if (Frame.getByHandle(handle))
	return;
    var parameterString = "";
    if (typeof parameter != "undefined")
    {
      if (parameter.constructor == Array)
      {
        for (var index = 0; index < parameter.length; index++)
          parameterString = parameterString + (index > 0 ? "&" : "") + "parameter" + (index + 1).toString() + "=" +
		parameter[index];
      } else
        parameterString = "parameter=" + parameter;
    }
    executeAJAX(page_url + "?design=JSON" + (parameterString != "" ? "&" + parameterString : ""), function 
	process(value)
    {
       if (value.charAt(0) != "{")
       {
	 alert(value);
	 document.body.removeChild(frameObject);
	 return;
       }

        document.body.appendChild(frameObject);

	var jsonValue = eval("(" + value + ")");
	frameInformation = jsonValue.frame;

	if (jsonValue.frame.modal && jsonValue.frame.modal == "true")
	{
	  var modalBackdrop = document.createElement("div");
	  modalBackdrop.className = "modalBackdrop";
	  modalBackdrop.contextMenu = null;
	  modalBackdrop.style.zIndex = "998";
	  modalBackdrop.setAttribute("id", "modalBackdrop");
	  document.body.appendChild(modalBackdrop);
	  frameObject.style.zIndex = "999";
	  isModal = true;
	} else
	{
	  layer = Frame.currentTopLayer;
	  Frame.currentTopLayer++;
	}

	if (jsonValue.frame.title || (jsonValue.frame.canClose && jsonValue.frame.canClose == "true") || 
		(jsonValue.frame.canMove && jsonValue.frame.canMove == "true"))
	{
	  titleBar = document.createElement("div");
	  titleBar.setAttribute("id","title-" + handle);
	  titleBar.className = "titleBar";
	  disableSelection(titleBar);
	  if (jsonValue.frame.canClose && jsonValue.frame.canClose == "true")
	  {
	    var closeButton = document.createElement("div");
	    closeButton.className = "frameButton close";
	    closeButton.onclick = function() { this.parentNode.parentNode.frameClass.close(); return false; };
	    titleBar.appendChild(closeButton);
	  }
	  if (jsonValue.frame.title)
	  {
	    var title = document.createElement("div");
	    title.className = "title" + (jsonValue.frame.canClose && jsonValue.frame.canClose == "true" ? " close" : "");
	    title.innerHTML = jsonValue.frame.title;
//	    title.setAttribute("id", "title-title-" + handle);
	    titleBar.appendChild(title);
	    titleBar.textElement = title;
	  }
	  frameObject.insertBefore(titleBar, frameContent);
	}
	
	if (jsonValue.frame.toolbar)
	{
	  var frameToolbar = document.createElement("div");
	  frameToolbar.className = "toolbar";
	  frameObject.insertBefore(frameToolbar, frameContent);
	  for (var toolbarItem in jsonValue.frame.toolbar)
	  {
	    var frameToolbarItem = document.createElement("div");
		frameToolbarItem.className = "item";
		frameToolbarItem.innerHTML = toolbarItem;
		frameToolbarItem.menu = new Menu();
		frameToolbarItem.menu.close();
		for (var menuItem in jsonValue.frame.toolbar[toolbarItem])
		{
		  var menuItemButton = MenuItem.createButton(menuItem.text, menuItem.icon);
		  //menuItemButton.setInteraction(function() { eval(menuItem.action); });
		  frameToolbarItem.menu.addItem(menuItemButton);
  		}
		frameToolbarItem.onclick = function(e)
		{
			var e = e || window.event;
			if (frameToolbarItem.menu.isOpen())
			  frameToolbarItem.menu.close();
			else
			{
				var currentItem = frameToolbarItem;
			    var parentX = 0;
				var parentY = currentItem.offsetHeight;
				while (currentItem)
				{
				  parentX += currentItem.offsetLeft;
				  parentY += currentItem.offsetTop;
				  currentItem = currentItem.offsetParent;
				}
			  frameToolbarItem.menu.setTop(parentY + "px");
			  frameToolbarItem.menu.setLeft(parentX + "px");
			  frameToolbarItem.menu.open();
			}
		};
		frameToolbar.appendChild(frameToolbarItem);
	  }
	}

	if (jsonValue.frame.sidebar)
	{
	  var frameSidebar = document.createElement("div");
	  frameSidebar.className = "sidebar " + jsonValue.frame.sidebar;
	  frameObject.insertBefore(frameSidebar, frameContent);
	  frameObject.frameClass.sidebar = frameSidebar;
	}

	frameObject.className = 'frameBase' + (jsonValue.frame.className != undefined ?
		' ' + jsonValue.frame.className : '');
	if (jsonValue.frame.width != undefined)
	{
	  frameObject.style.width = jsonValue.frame.width;
	}
	
	if (jsonValue.frame.height != undefined)
	{
	  frameContent.style.height = jsonValue.frame.height;
	  frameObject.style.height = (frameContent.clientHeight + (frameToolbar ? frameToolbar.offsetHeight : 0) +
		(title ? title.offsetHeight : 0)) + "px";
	  if (frameSidebar)
	    frameSidebar.style.height = frameContent.style.height;
	}

	if (jsonValue.frame.canMove && jsonValue.frame.canMove == "true")
	{
	  Drag.init(title, frameObject, 0, document.documentElement.clientWidth - frameObject.offsetWidth,
		0, document.getElementById("background-image").clientHeight - frameObject.offsetHeight -
		Taskbar.taskbarContainer.offsetHeight);
	   canMove = true;
	}

	if (!jsonValue.frame.position || jsonValue.frame.position == "centered")
	{
	  frameObject.style.position = "absolute";
	  frameObject.style.top = ((document.getElementById("background-image").clientHeight -
		(Taskbar.taskbarContainer ? Taskbar.taskbarContainer.offsetHeight : 0) -
		frameObject.offsetHeight) / 2).toString() + "px";
	  frameObject.style.left = ((document.getElementById("background-image").clientWidth -
		frameObject.offsetWidth) / 2).toString() + "px";
	  if (jsonValue.frame.position == "centered")
	  {
	    verticalCentering = true;
	    horizontalCentering = true;
	  }
	} else if (jsonValue.frame.position == "absolute")
	{
	  frameObject.style.position = jsonValue.frame.position;
	  if (jsonValue.frame.top != undefined)
	    frameObject.style.top = jsonValue.frame.top;
	  if (jsonValue.frame.bottom != undefined)
	    frameObject.style.bottom = jsonValue.frame.bottom;
	  if (jsonValue.frame.left != undefined)
	    frameObject.style.left = jsonValue.frame.left;
	  if (jsonValue.frame.right != undefined)
	    frameObject.style.right = jsonValue.frame.right;
	}

	if (jsonValue.frame.onClose)
	  frameCloseFunction = jsonValue.frame.onClose;

	if (!jsonValue.frame.onTaskbar || jsonValue.frame.onTaskbar == 'true')
	{
	  onTaskbar = true;
	  Taskbar.createItem(frameObject.frameClass);
	} else
	  onTaskbar = false;

	if (typeof load_contents != "undefined" && !load_contents)
	{
	  frameObject.frameClass.focus();
	  if (loaded_callback)
	    loaded_callback(frameObject.frameClass);
	  return;
	}
	
        executeAJAX(page_url + (parameterString ? "?" + parameterString : ""), function processTwo(contentValue, responseInfo)
        {
                frameContent.innerHTML = "<span style=\"display:none;\">hi</span>" + contentValue; //IE requires some HTML before script/style elements
                var codeBlocks = frameContent.getElementsByTagName("script");
                for (var scriptIndex = 0; scriptIndex < codeBlocks.length; scriptIndex++)
                        eval(codeBlocks[scriptIndex].text);
                frameObject.frameClass.focus();
                if (loaded_callback)
                  loaded_callback();
        });
    });
  };
  this.loadSidebar = function(call, callback)
  {
    if (!this.sidebar)
      return;
    executeAJAX(call, function(results)
    {
      frameObject.frameClass.sidebar.innerHTML = "<span style=\"display:none;\">hi</span>" + results;
      
      if (callback)
        callback();
    });
  };
  var frameCloseFunction = null;
  this.close = function()
  {
    document.body.removeChild(frameObject);
    Frame.currentTopLayer--;
    if (Frame.currentTopLayer < 1)
      Frame.currentTopLayer = 1;
    var allFrames = Frame.getAll();
    for (var index = 0; index < allFrames.length; index++)
    {
      if (allFrames[index].getLayer() <= layer)
	continue;
      allFrames[index].setLayer(allFrames[index].getLayer() - 1);
    }
    if (frameCloseFunction)
      eval(frameCloseFunction);
    if (isModal)
      document.body.removeChild(document.getElementById("modalBackdrop"));
    if (onTaskbar)
 	Taskbar.removeItem(frameObject.frameClass);
  };
}
Frame.currentTopLayer = 1;
Frame.getByHandle = function(search_handle)
  {
    var returnValue = document.getElementById("frame-" + search_handle);
    if (!returnValue)
      return null;
    return returnValue.frameClass;
  };
Frame.getAll = function()
  {
    var allDivElements = document.getElementsByTagName("div");
    var allFrames = new Array();
    for (var index = 0; index < allDivElements.length; index++)
      if (allDivElements[index].frameClass)
        allFrames = allFrames.concat(allDivElements[index].frameClass);
    return allFrames;
  };
