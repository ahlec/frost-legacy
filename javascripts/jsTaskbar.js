function Taskbar() { }
Taskbar.taskbarContainer = null;
Taskbar.profilePicture = null;
Taskbar.programsButton = null;

Taskbar.accountButton = null;
Taskbar.accountMenuOpen = false;
Taskbar.accountMenu = null;
Taskbar.mainMenu = null;
Taskbar.programsMenu = null;
Taskbar.userMenu = null;
Taskbar.taskbar = null;

Taskbar.create = function()
{
  var container = document.createElement("div");
  container.setAttribute("id", "taskbarContainer");
  container.className = "taskbarContainer";
  Taskbar.taskbarContainer = container;
  document.body.appendChild(Taskbar.taskbarContainer);

  var accountMenu = new Menu();
  accountMenu.close();
  accountMenu.setBottom("90px");
  accountMenu.setLeft("6px");

  Taskbar.programsMenu = new Menu({}, 0);
  
  accountMenu.addItem(MenuItem.createSubmenu("Programs", "images/icons/programs.png", Taskbar.programsMenu));

  Taskbar.userMenu = new Menu({}, 0);
  accountMenu.addItem(MenuItem.createSubmenu("Account", "images/icons/frost-account.png", Taskbar.userMenu));

  var logoutButton = MenuItem.createButton("Logout", "images/icons/logout.png");
  logoutButton.setInteraction(function()
  {
    Prompt("Are you ready to logout?", "Logout?", [
                {text: 'Yes', colour:'green', action: function() { logout(); }},
                {text: 'No', colour:'red', action: function() {} }
        ]);	  
  });
  accountMenu.addItem(logoutButton);
  Taskbar.accountMenu = accountMenu;

  var profilePicture = document.createElement("img");
  profilePicture.className = "profilePicture";
  profilePicture.setAttribute("id", "profilePicture");
  profilePicture.src = 'http://www.gravatar.com/avatar/' + Profile.emailHash;
  //profilePicture.src = 'images/user-icons/get.php?uid=' + Profile.uID.toString();
  profilePicture.onclick = Taskbar.toggleMenu;
  profilePicture.base = { };
  profilePicture.base.getContextInfo = function() { return { staticMenu:true, menu:Taskbar.accountMenu }; };
  Taskbar.profilePicture = profilePicture;
  Taskbar.taskbarContainer.appendChild(Taskbar.profilePicture);

  var taskbar = document.createElement("div");
  taskbar.setAttribute("id", "taskbarTaskbar");
  taskbar.className = "taskbar";
  Taskbar.taskbarContainer.appendChild(taskbar);
  Taskbar.taskbar = taskbar;

  /*var mainMenu = new Menu();
  mainMenu.close();
  mainMenu.setBottom("90px");
  mainMenu.setLeft("100px");
  Taskbar.mainMenu = mainMenu;

  var programsButton = document.createElement("img");
  programsButton.setAttribute("id", "taskbarProgramsButton");
  programsButton.className = "taskbarProgramsButton";
  programsButton.src = 'images/taskbarIcon-programs.png';
  programsButton.onclick = Taskbar.toggleMenu;
  programsButton.base = { };
  programsButton.base.getContextInfo = function() { return { staticMenu:true, menu:Taskbar.mainMenu }; };
  Taskbar.programsButton = programsButton;
  Taskbar.taskbarContainer.appendChild(Taskbar.programsButton);*/

};

Taskbar.toggleMenu = function(e)
{
  if ((this == Taskbar.profilePicture && Taskbar.accountMenu.isOpen()) || (this == Taskbar.programsButton &&
	Taskbar.mainMenu.isOpen()))
    return;
  this.rightClick();
    if (!e)
      var e = window.event;
    if (e)
      e.cancelBubble = true;
    if (e.stopPropagation)
      e.stopPropagation();
};
Taskbar.createItem = function(frame)
{
  if (document.getElementById("taskbar-item-" + frame.getHandle()))
  {
    alert("there is already a taskbar item with this handle ('" + frame.getHandle() + "')");
    return;
  }
  var taskbarItem = document.createElement("div");
  taskbarItem.className = "item";
  taskbarItem.setAttribute("id", "taskbar-item-" + frame.getHandle());
  taskbarItem.frame = frame;
//  taskbarItem.innerHTML = frame.getTitle();
  taskbarItem.onclick = frame.focus;

  var taskbarItemIcon = document.createElement("img");
  taskbarItemIcon.className = "icon";
  taskbarItemIcon.isComponent = true;
  taskbarItemIcon.src = web_path + "/" + (typeof frame.getIconSmall() != "undefined" ? frame.getIconSmall() : 
	"images/taskbarIcon-frame.png");
  taskbarItem.appendChild(taskbarItemIcon);

  var taskbarItemName = document.createElement("div");
  taskbarItemName.className = "name";
  taskbarItemName.innerHTML = frame.getTitle();
  taskbarItemName.isComponent = true;
  taskbarItem.appendChild(taskbarItemName);
  taskbarItem.name = taskbarItemName;

  Taskbar.taskbar.appendChild(taskbarItem);
  Taskbar.focus(frame);
};
Taskbar.getItems = function()
{
  var allItems = Taskbar.taskbar.getElementsByTagName("div");
  var items = new Array();
  for (var index = 0; index < allItems.length; index++)
    if (allItems[index].className && allItems[index].className.indexOf("item") != -1 &&
	allItems[index].id.indexOf("taskbar-item-") != -1)
	items.push(allItems[index]);
  return items;
}
Taskbar.removeItem = function(frame)
{
  var taskbarItems = Taskbar.getItems();
  var removedItemTaskbarIndex = -1;
  for (var index = 0; index < taskbarItems.length; index++)
    if (taskbarItems[index].frame == frame)
      removedItemTaskbarIndex = index;
  Taskbar.taskbar.removeChild(document.getElementById("taskbar-item-" + frame.getHandle()));
  if (taskbarItems.length > 1)
  {
    if (taskbarItems.length > removedItemTaskbarIndex + 1)
      taskbarItems[removedItemTaskbarIndex + 1].frame.focus();
    else
      taskbarItems[removedItemTaskbarIndex - 1].frame.focus();
  }
};
Taskbar.updateItems = function(frame)
{
  var taskbarItems = Taskbar.getItems();
  for (var index = 0; index < taskbarItems.length; index++)
    if (taskbarItems[index].frame == frame)
    {
      taskbarItems[index].name.innerHTML = frame.getTitle();
    }
};
Taskbar.focus = function(frame)
{
  var taskbarItems = Taskbar.getItems();
  for (var index = 0; index < taskbarItems.length; index++)
    taskbarItems[index].className = "item" + (taskbarItems[index].frame == frame ? " focus" : "");
};
