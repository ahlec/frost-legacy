var SUP = null;
function File()
{
  var thisFile = this;
  var id = null;
  var name = null;
  var location = null;
  var size = null;
  var type = null;
  var rawContextMenu = null;
  this.getId = function() { return id; };
  this.getName = function() { return name; };
  this.getLocation = function() { return location; };
  this.getSize = function() { return size; };
  this.getType = function() { return type; };
  this.getRawContextMenu = function() { return rawContextMenu; };
  var registeredInterfaceIcons = new Array();
  this.setName = function(new_name)
  {
    name = new_name;
    for (var index = 0; index < registeredInterfaceIcons.length; index++)
      registeredInterfaceIcons[index].setName(new_name);
  };
  this.getRegisteredIcons = function() { return registeredInterfaceIcons; };
  this.registerInterfaceIcon = function(interfaceIcon) { registeredInterfaceIcons.push(interfaceIcon); };
  this.removeInterfaceIcon = function(interfaceIcon) { registeredInterfaceIcons = registeredInterfaceIcons.filter(function(element,
	index, array)
	{
	  return (element != interfaceIcon);
	}); };
  this.load = function(file_id, on_load)
  {
    for (var index = 0; index < File.files.length; index++)
      if (Files.files[index].getId() == file_id)
      {
	SUP = thisFile;
	alert("found");
	return;
      }
    executeAJAX("scripts/get-file-information.php?identity=" + file_id, function process(results)
    {
      if (results == "")
      {
        alert("File does not exist or does not belong to you.");
        return;
      }
      var fileInformation = eval("(" + results + ")");
      id = fileInformation.id;
      name = fileInformation.name;
      location = fileInformation.location;
      size = fileInformation.size;
      type = fileInformation.type;
      rawContextMenu = fileInformation.contextMenu;
      File.files.push(thisFile);
      if (on_load)
        on_load();
    });
  };
}
File.files = new Array();
File.get = function(identity)
{
  for (var index = 0; index < File.files.length; index++)
    if (File.files[index].getId() == identity)
      return File.files[index];
  return null;
}
