function Browser() { };
Browser.icon = null;

Browser.initialize = function()
{
  Browser.resetIcon();
/*  Browser.icon = document.createElement("link");
  Browser.icon.rel = "icon";
  document.getElementsByTagName("head")[0].appendChild(Browser.icon);
  Browser.resetIcon();*/
}
Browser.resetIcon = function()
{
  Browser.changeIcon("images/filesystem.ico");
//  document.getElementsByTagName("head")[0]
//  Browser.icon.href = "images/filesystem.ico";
}
Browser.changeIcon = function(icon_file)
{
  if (Browser.icon)
    document.getElementsByTagName("head")[0].removeChild(Browser.icon);
  Browser.icon = document.createElement("link");
  Browser.icon.rel = "icon";
  Browser.icon.href = icon_file;
  document.getElementsByTagName("head")[0].appendChild(Browser.icon);
}
Browser.testSelectionStart = function(targetElement)
{
  if (!targetElement || !targetElement.nodeName)
    return false;
  return (targetElement.nodeName == "INPUT" || targetElement.nodeName == "TEXTAREA" || targetElement.nodeName == "BUTTON");
};

 if (document.onselectstart)
   document.onselectstart = function(e)
   {
     if (!e)
       var e = window.event;
     var targetElement = (e.target ? e.target : e.srcElement);
   return Browser.testSelectionStart(targetElement);
   };
 document.onmousedown = function(e)
 {
   if (!e)
     var e = window.event;
   var targetElement = (e.target ? e.target : e.srcElement);
   return Browser.testSelectionStart(targetElement);
 };