function FramePane(parentFrame, handle)
{
  var title = null;
  var icon = null;

  this.setTitle = function(new_title)
  {
    title = new_title;
  };
  this.getTitle = function() { return title; };
  this.getHandle = function() { return handle; };
  this.setIcon = function(new_icon)
  {
    icon = new_icon;
  };

  this.container = document.createElement("div");
  this.container.className = "frameContent";
//  if (typeof parentFrame.paneInformation.width != "undefined")
//    this.container.style.width = parentFrame.paneInformation.width + "px";
  if (typeof parentFrame.paneInformation.height != "undefined")
    this.container.style.height = parentFrame.paneInformation.height + "px";
  this.container.style.display = "none";
  parentFrame.paneContainer.appendChild(this.container);
  var container = this.container;

  this.load = function(ajax, loaded_callback)
  {
        executeAJAX(ajax, function processTwo(contentValue, responseInfo)
        {
                container.innerHTML = "<span style=\"display:none;\">hi</span>" + contentValue; //IE requires some HTML before script/style elements
                var codeBlocks = container.getElementsByTagName("script");
                for (var scriptIndex = 0; scriptIndex < codeBlocks.length; scriptIndex++)
                        eval(codeBlocks[scriptIndex].text);
                //frameObject.frameClass.focus();
                if (loaded_callback)
                  loaded_callback();
        });
  };
  this.display = function(value)
  {
    container.style.display = (value ? "block" : "none");
    if (value && parentFrame.getTitleBarElement())
      parentFrame.getTitleBarElement().textElement.innerHTML = title;
  };
  this.show = function() { this.display(true); };
  this.hide = function() { this.display(false); };
}
