IconItem.itemWithFocus = null;
function IconItem(icon, name, context_handles, defaultProgram, item_type, gID, itemLocation, locationContentsGroup)
{
  this.onExecute = null;
  var position = null;

  var uID = IconItem.uniqueID;
  var iconItem = this;
  IconItem.uniqueID++;

  if (defaultProgram && defaultProgram.length > 0)
    context_handles.push("default:" + defaultProgram);

  IconItem.allItems.push(this);

  this.getPosition = function() { return position; };
  this.setPosition = function(new_position) { position = new_position; };
  this.getUId = function() { return uID; };  
  this.getContextInfo = function() { return {gid: gID}; };
  this.getDropInfo = function() { return {takes: (item_type == "location"), gid: gID}; };
  this.getGId = function() { return gID; };
  this.getLocation = function() { return itemLocation; };
  this.getLocationContentsGroup = function() { return locationContentsGroup; };

  this.destroy = function()
  {
    if (IconItem.itemWithFocus == iconItem)
      iconItem.unfocus();
    iconItem.container.parentNode.removeChild(iconItem.container);
    if (IconItem.allItems.indexOf(iconItem) > -1)
      IconItem.allItems.splice(IconItem.allItems.indexOf(iconItem), 1);
    iconItem = null;
  };

  this.focus = function()
  {
    if (IconItem.itemWithFocus == iconItem)
      return;
    if (IconItem.itemWithFocus != null)
    {
      IconItem.itemWithFocus.container.className = "itemContainer icon";
      if (IconItem.itemWithFocus.getLocationContentsGroup() && locationContentsGroup &&
        IconItem.itemWithFocus.getLocationContentsGroup() != locationContentsGroup && IconItem.itemWithFocus.getLocationContentsGroup().onblur)
        IconItem.itemWithFocus.getLocationContentsGroup().onblur();
    }
    iconItem.container.className = "itemContainer icon focus";
    IconItem.itemWithFocus = iconItem;
    if (locationContentsGroup && locationContentsGroup.onselection)
      locationContentsGroup.onselection(iconItem);
    return;
  };

  this.unfocus = function()
  {
    if (IconItem.itemWithFocus != iconItem)
      return;
    iconItem.container.className = "itemContainer icon";
    if (locationContentsGroup && locationContentsGroup.onblur)
      locationContentsGroup.onblur();
    IconItem.itemWithFocus = null;
  };

  this.execute = function(e)
  {
    if (this.nodeName && this.nodeName == "DIV")
    {
      this.interfaceItem.execute();
      return;
    }
    if (!defaultProgram || defaultProgram.length == 0)
      return true;
    var evaled = eval(defaultProgram + ".open");
    evaled(this.getContextInfo());
    return true;
  };

  var group = null;
  this.setGroup = function(new_group) { group = new_group; };
  this.getGroup = function() { return group; };

  this.setName = function(new_name)
  {
    itemLabel.innerHTML = new_name;
    itemContainer.title = new_name;
  };
  this.getName = function() { return itemContainer.title; };

  var itemContainer = document.createElement("div");
  itemContainer.base = this;
  itemContainer.className = "itemContainer icon";
  itemContainer.ondblclick = this.execute;
  itemContainer.onclick = this.focus;
  var mouseDown = false;
  var mouseOver = false;
  itemContainer.title = name;
  itemContainer.interfaceItem = this;
  itemContainer.contextHandles = context_handles;
  disableSelection(itemContainer);

  var itemBackground = document.createElement("div");
  itemBackground.className = "background";
  itemBackground.isComponent = true;
  itemContainer.appendChild(itemBackground);

  var itemIcon = document.createElement("div");
  itemIcon.className = "image";
  itemIcon.isComponent = true;

  var iconImage = new Image();
  iconImage.onload = function() { itemIcon.style.backgroundImage = "url(" + iconImage.src + ")"; };
  iconImage.src = (icon && icon.length > 0 ? web_path + "/images/file-icons/" + icon : web_path + "/images/file-icons/unknown.png");

  this.setIcon = function(new_icon)
  {
    iconImage.src = (new_icon && new_icon.length > 0 ? web_path + "/images/file-icons/" + new_icon : web_path +
	"/images/file-icons/unknown.png");
  }

  itemContainer.appendChild(itemIcon);

  var itemLabel = document.createElement("div");
  itemLabel.className = "label";
  itemLabel.innerHTML = name;
  itemLabel.isComponent = true;
  itemContainer.appendChild(itemLabel);
 
  itemContainer.onmousedown = function(e)
  {
    if (!e)
      var e = window.event;
    if ((e.which && e.which != 1) || (!e.which && e.button != 1))
      return false;
    mouseDown = true;
    this.base.focus();
    return false;
  };
  itemContainer.onmouseout = function() { mouseOver = false; };
  itemContainer.onmouseover = function() { mouseOver = true; };
  
  this.isDragging = function() { return (mouseDown && !mouseOver); };
  this.endDrag = function() { mouseDown = false; };

  var keyboardActions = new Array();
  this.activateKeyFunction = function(keyCode)
  {
    for (var index = 0; index < keyboardActions.length; index++)
      if (keyboardActions[index].keyCode == keyCode)
        keyboardActions[index].action.call(this.underlying, 
		this.underlying);
  };

  this.container = itemContainer;
}
IconItem.uniqueID = 0;
IconItem.allItems = new Array();
IconItem.getItemsByGId = function(gID)
{
  var items = new Array();
  for (var index = 0; index < IconItem.allItems.length; index++)
    if (IconItem.allItems[index].getContextInfo().gid == gID)
      items.push(IconItem.allItems[index]);
  return items;
};

document.addEventListener('click', IconItemDocumentOnclick, false);
function IconItemDocumentOnclick(e)
{
  if (!e)
    var e = window.event;
  var clickedElement = (e.target ? e.target : e.srcElement);

  if (!IconItem.itemWithFocus)
    return true;

  while (clickedElement)
  {
    if (clickedElement.interfaceItem)
      break;
    clickedElement = clickedElement.parentNode;
  }
  if (!clickedElement || IconItem.itemWithFocus != clickedElement.interfaceItem)
    IconItem.itemWithFocus.unfocus();
}

document.onmouseup = function(e)
{
  if (!e)
    var e = window.event;
  var destinationElement = (e.target ? e.target : e.srcElement);
  if (!IconItem.itemWithFocus || !IconItem.itemWithFocus.isDragging())
    return true;
  while (destinationElement)
  {
    if (destinationElement.base && destinationElement.base.getDropInfo && destinationElement.base.getDropInfo().takes)
      break;
    destinationElement = destinationElement.parentNode;
  }
  IconItem.itemWithFocus.endDrag();
  if (destinationElement && destinationElement.base && destinationElement.base.getDropInfo)
  {
    if (destinationElement.base.getDropInfo().gid == IconItem.itemWithFocus.getLocation())
      return true;
    alert('hi');
    if (destinationElement.base.getDropInfo().gid == 2)
      BasicFileOperations.moveToTrash(IconItem.itemWithFocus.getContextInfo());
    else
      moveItem(IconItem.itemWithFocus.getGId(), destinationElement.base.getDropInfo().gid);
  }
};

window.onkeydown = function(e)
{
  if (!IconItem.itemWithFocus)
    return;
  if (!e)
    var e = window.event;
  var depressedKey = e.keyCode;
  if (depressedKey != 37 && depressedKey != 39 && depressedKey != 13)
  {
    IconItem.itemWithFocus.activateKeyFunction(depressedKey);
    return;
  }
  if (depressedKey == 13)
  {
    IconItem.itemWithFocus.execute();
    return false;
  }
  var currentIndex = IconItem.itemWithFocus.getGroup().indexOf(IconItem.itemWithFocus);
  if (depressedKey == 37)
    currentIndex--;
  if (depressedKey == 39)
    currentIndex++;
  if (currentIndex >= 0 && currentIndex < IconItem.itemWithFocus.getGroup().length)
  {
    IconItem.itemWithFocus.getGroup().get(currentIndex).focus();
  }
}