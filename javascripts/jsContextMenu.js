function ContextMenu() { };
ContextMenu.menu = null;
ContextMenu.initialized = false;
ContextMenu.items = null;

ContextMenu.initialize = function()
{
  executeAJAX("scripts/get-context-menu.php", function load(items)
  {
    ContextMenu.items = eval("(" + items + ")");
    ContextMenu.initialized = true;
  });
}
ContextMenu.close = function()
{
  if (!ContextMenu.menu)
    return;
  ContextMenu.menu.close();
  ContextMenu.menu = null;
};

ContextMenu.create = function(context_handles, item_information)
{
  var items = new Array();
  if (typeof item_information == "undefined")
    var item_information = {};
  for (var index = 0; index < context_handles.length; index++)
  {
    if (context_handles[index].indexOf("default:") > -1)
    {
      var program = Programs.getByHandle(context_handles[index].substr(8));
      if (program != null)
      {
        var default_action = {icon: program.icon, text: "Open", action: program.handle + ".open", default: true };
        items = [default_action].concat(items);
      }
    } else
      items = items.concat(ContextMenu.items.filter(function(element, elementIndex, array)
	{
          if (!element)
            return false;
	  if (element.conditional != undefined && element.conditional != null && eval(element.conditional))
	    if (!eval(element.conditional).call(undefined, item_information))
		return false;
	  return (element.contextHandles.indexOf(context_handles[index]) != -1);
	}));
  }
  items = items.filter(function(element, index, array) { return (array.indexOf(element) == index); });
  items = items.sort(function(a, b)
  {
    if (a.default)
      return -1;
    if (b.default)
      return 1;
    return (b.weight - a.weight);
  });
  return items;
};

var latestmenu;
function setupContextMenu()
{
  document.body.oncontextmenu = function (e)
  {
    if (!ContextMenu.initialized)
      return false;

    if (!e)
      var e = window.event;
	  
    var targetObject = (e.target || e.srcElement);
    while (targetObject && targetObject.isComponent)
	{
      targetObject = targetObject.parentNode;
	}

    if (!targetObject)
    {
      ContextMenu.close();
      return false;
    }

    if (targetObject.base && targetObject.base.focus)
      targetObject.base.focus();

    var contextInfo = (targetObject.base && targetObject.base.getContextInfo ? targetObject.base.getContextInfo() : undefined);

    if (contextInfo && typeof contextInfo.staticMenu != "undefined" && contextInfo.staticMenu == true)
      var newMenu = contextInfo.menu;
    else
    {
      var contextHandles = targetObject.contextHandles || [];
      if (targetObject.localName == "input" || targetObject.localName == "button")
      {
        if (targetObject.type == "text" || targetObject.type == "password")
          contextHandles = contextHandles.concat('input-text');
        if (targetObject.localName == "button" || targetObject.type == "button" || targetObject.type == "submit" || 
		targetObject.type == "cancel")
          contextHandles = contextHandles.concat('input-button');
      }

      if (contextHandles.length == 0)
      {
        ContextMenu.close();
        return false;
      }
    }

    if (ContextMenu.menu)
      ContextMenu.close();

    if (typeof newMenu == "undefined")
    {
      var contextMenuItems = ContextMenu.create(contextHandles, contextInfo);
      if (contextMenuItems.length == 0)
      {
        ContextMenu.close();
        return false;
      }
    }

    ContextMenu.menu = newMenu || new Menu(contextMenuItems, contextInfo);
    if (typeof newMenu == "undefined")
    {
      var newLeft = (e.clientX + ContextMenu.menu.getWidth() >=
		document.getElementById("background-image").clientWidth ? e.clientX - ContextMenu.menu.getWidth() :
		e.clientX);
      var newTop = (e.clientY + ContextMenu.menu.getHeight() >=
		document.getElementById("background-image").clientHeight -
		(document.getElementById("taskbarContainer") ?
		document.getElementById("taskbarContainer").clientHeight : 0) ?	e.clientY -
		ContextMenu.menu.getHeight() : e.clientY);
	  ContextMenu.menu.setLeft(newLeft + "px");
      ContextMenu.menu.setTop(newTop + "px");
    }

    ContextMenu.menu.setLayer(1001);
    ContextMenu.menu.open();
    return false;
  };

  document.body.onclick = function (e)
  {
    if (ContextMenu.menu == null || !ContextMenu.menu.isOpen())
      return true;
    if (!e)
      e = window.event;

    var previousObject = null;
    var targetObject = (e.target ? e.target : e.srcElement);
    while (targetObject)
    {
      if (targetObject.menu == ContextMenu.menu)
      {
        ContextMenu.close();
        if (previousObject)
          previousObject.click();
        return true;
      }
      previousObject = targetObject;
      targetObject = targetObject.parentNode;
    }
    ContextMenu.close();
    return true;

    var previousObject = null;
    var targetObject = (e.target ? e.target : e.srcElement);
    while (targetObject)
    {
      if (targetObject.id == "menu-" + contextMenu.getHandle())
      {
        contextMenu.close();
        if (previousObject)
          previousObject.click();
        return true;
      }
      previousObject = targetObject;
      targetObject = targetObject.parentNode;
    }
    contextMenu.close();
    contextMenu = null;
    return true;
  }
}
