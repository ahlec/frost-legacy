function Profile() { };
Profile.Name = null;
Profile.Theme = null;
Profile.uID = null;
Profile.emailHash = null;
Profile.admin = null;

Profile.load = function(user_information)
{
  Profile.Name = user_information.username;
  Profile.Theme = user_information.theme;
  Profile.uID = user_information.id;
  Profile.emailHash = user_information.emailHash;
  Profile.admin = (user_information.admin == "true");
}
