if (!Function.prototype.bind)
  Function.prototype.bind = function( obj ) {
    if(typeof this !== 'function') // closest thing possible to the ECMAScript 5 internal IsCallable function
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');

    var slice = [].slice,
        args = slice.call(arguments, 1), 
        self = this, 
        nop = function () {}, 
        bound = function () {
          return self.apply( this instanceof nop ? this : ( obj || {} ), 
                              args.concat( slice.call(arguments) ) );    
        };

    bound.prototype = this.prototype;

    return bound;
  };
if (!Array.prototype.filter)
  Array.prototype.filter = function(testfunction)
  {
    var newArray = new Array();
    for (var index = 0; index < this.length; index++)
      if (testfunction(this[index], index, this))
        newArray.push(this[index]);
    return newArray;
  };
if (!Array.prototype.indexOf)
  Array.prototype.indexOf = function(targetElement, startIndex)
  {
    if (typeof startIndex == "undefined")
      var startIndex = 0;
    for (var index = startIndex; index < this.length; index++)
      if (this[index] == targetElement)
        return index;
    return -1;
  };
if (!document.createEvent)
  document.createEvent = function(eventType) { return document.createEventObject(eventType); };
if (!String.prototype.concat)
  String.prototype.concat = function()
  {
alert('hi');
/*
    var newString = this;
    for (var index = 0; index < arguments.length; index++)
      newString += arguments[index];
    alert(newString);
    return newString;*/
  };