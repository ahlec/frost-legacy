function Theme() { };
Theme.isSetup = false;
Theme.setup = function()
{
  if (Theme.isSetup)
    return;
  var backgroundImage = document.createElement("div");
  backgroundImage.setAttribute('id', 'background-image');
  backgroundImage.className = 'backgroundImage';
  var behindBackgroundImage = document.createElement("div");
  behindBackgroundImage.setAttribute('id', 'background-fallback');
  behindBackgroundImage.className = 'backgroundImage';
  disableSelection(backgroundImage);
  document.body.appendChild(behindBackgroundImage);
  document.body.appendChild(backgroundImage);
  Theme.isSetup = true;
}
Theme.applyingTheme = false;
Theme.currentTheme = null;
Theme.apply = function(theme_id)
{
  if (Theme.applyingTheme)
    return;
  Theme.currentTheme = theme_id;
  Theme.applyingTheme = true;
  executeAJAX("scripts/get-theme-information.php?theme=" + theme_id,
    function process(results)
	{
	  if (results == "")
	  {
	    alert("Specified theme no longer exists. Reverting to default theme.");
	    Theme.applyingTheme = false;
	    Theme.apply(1);
	    return;
	  }
	  var themeInformation = eval("(" + results + ")");
	  document.getElementById("theme-stylesheet").href = web_path + "/themes/skins/" +
		themeInformation.stylesheet;
	  document.getElementById('background-fallback').style.backgroundImage =
		document.getElementById('background-image').style.backgroundImage;
	  document.getElementById('background-image').style.display = 'none';
	  var newBackgroundImage = new Image();
	  newBackgroundImage.onload = function()
	  {
	    document.getElementById('background-image').style.backgroundImage = "url(" +
		newBackgroundImage.src + ")";
	    fadeIn("background-image");
	    Theme.applyingTheme = false;
	  };
	  newBackgroundImage.src = web_path + "/themes/backgrounds/" + themeInformation.handle + ".jpg";
	});
};
