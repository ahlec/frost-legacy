/* Prompt */
function Prompt(question, caption, buttons)
{
  var promptFrame = new Frame("prompt");
  var promptPHPParameters = [question, caption, buttons.length];
  for (var index = 0; index < buttons.length; index++)
    promptPHPParameters.push(buttons[index].text, buttons[index].colour);
  promptFrame.load("screens/prompt.php", promptPHPParameters, function assign()
	{
	  for (var button = 1; button <= buttons.length; button++)
	  {
	    document.getElementById("promptButton-" + button).index = button - 1;
	    document.getElementById("promptButton-" + button).onclick = function(e)
	    {
		if (!e)
		  var e = window.event;
		var index = (e.target ? e.target : e.srcElement).index;
		buttons[index].action();
		Frame.getByHandle("prompt").close();
	    };
	  }
          document.getElementById("promptButton-1").focus();
	});
}

/* Selection Management */
function disableSelection(element)
{

return; //obsolete

  if (element.onselectstart)
    element.onselectstart = function() { return false; };
  else if (element.style.MozUserSelect)
    element.style.MozUserSelect = "none";
  else
    element.onmousedown = function() { return false; };
}
