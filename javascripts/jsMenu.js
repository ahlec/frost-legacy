function Menu(jsonBase, gID)
{
  var menuObject = document.createElement("div");
  disableSelection(menuObject);
  menuObject.menu = this;
  menuObject.style.display = "none";
  menuObject.className = "menuContainer";
  menuObject.onmouseout = function(e) {
    if (!menuObject.menu.parentMenu)
      return true;
    if (!e)
      var e = window.event;
      var priorTrail = trail.slice();
    var destinationElement = e.toElement || e.relatedTarget;
    while (destinationElement)
    {
      if (destinationElement.menu)
      {
        var destMenu = destinationElement.menu;
        while (destMenu)
        {
          if (destMenu == menuObject.menu)
          {
            var toElement = e.toElement || e.relatedTarget;
            var fromElement = e.target || e.srcElement || e.fromElement;
            var srcElement = e.target || e.srcElement || e.fromElement;
            var srcMenu = null;
            while (srcElement)
            {
              if (srcElement.menu)
              {
                srcMenu = srcElement.menu;
                break;
              }
              srcElement = srcElement.parentNode;
            }
            while (toElement && !toElement.menuItem)
              toElement = toElement.parentNode;
            while (fromElement && !fromElement.menuItem)
              fromElement = fromElement.parentNode;
              
            if (!toElement || !toElement.menuItem)
            {
              menuObject.menu.close();
              return;
            }
            if (toElement.menuItem.type == "submenu" && toElement.menuItem.submenu == srcMenu)
              return;
            if (fromElement && fromElement.menuItem && fromElement.menuItem.type == "submenu" && fromElement.menuItem.submenu == destMenu)
              return;
            if (fromElement && fromElement.menuItem.type == "submenu")
            {
              fromElement.menuItem.submenu.close();
              return;
            }
            return;
          }
          destMenu = destMenu.parentMenu;
        }
      }
      destinationElement = destinationElement.parentNode;
    }
    menuObject.menu.close();
  };
  document.body.appendChild(menuObject);
  var menuItems = new Array();
  var _open = false;
  this.parentMenu = null;

  this.setLayer = function(layer) { menuObject.style.zIndex = layer; };
  this.setTop = function(location) { menuObject.style.top = location; };
  this.setBottom = function(location) { menuObject.style.bottom = location; };
  this.setRight = function(location) { menuObject.style.right = location; };
  this.setLeft = function(location) { menuObject.style.left = location; };
  this.setWidth = function(size) { menuObject.style.width = size; };
  this.length = function() { return menuItems.length; };
  this.item = function(index) { return menuItems[index]; };
  this.getGID = function() { return gID; };
  this.getWidth = function() {
	menuObject.style.visibility = 'hidden';
	menuObject.style.display = 'block';
	var width = menuObject.clientWidth;
	menuObject.style.display = 'none';
	menuObject.style.visibility = 'visible';
	return width; }; //menuObject.clientWidth; };
  this.getHeight = function() {
	menuObject.style.visibility = 'hidden';
	menuObject.style.display = 'block';
	var height = menuObject.clientHeight;
	menuObject.style.display = 'none';
	menuObject.style.visibility = 'visible';
	return height; };
  this.isOpen = function() { return _open; };
  
  this.removeItem = function(menuItem)
  {
    
  };
  this.addItem = function(menuItem, index)
  {
    if (index == undefined)
      var index = menuItems.length;
    if (index < 0)
      index = 0;
    else if (index > menuItems.length)
      index = menuItems.length - 1;
    var elementsUpToIndex = menuItems.slice(0, index);
    var elementsAfterIndex = menuItems.slice(index);
    menuItems = elementsUpToIndex.concat(menuItem, elementsAfterIndex);
    if (menuItems.length == 1 || index == menuItems.length - 1)
    {
      menuObject.appendChild(menuItem.getContainer());
      menuItem.getContainer().className += " last";
      if (menuItem.getContainer().previousSibling)
        menuItem.getContainer().previousSibling.className = menuItem.getContainer().previousSibling.className.replace(" last",
	  "");
    }
    else
      menuObject.insertBefore(menuItem.getContainer(),
	menuItems[index + 1].getContainer());
    menuItem.setParentMenu(this);
  };

  if (jsonBase)
  {
    for (var index = 0; index < jsonBase.length; index++)
    {
      if (jsonBase[index].action)
      {
	var menuButton = MenuItem.createButton(jsonBase[index].text, jsonBase[index].icon, gID, jsonBase[index].default);
	menuButton.setInteraction(eval(jsonBase[index].action));
	this.addItem(menuButton);
      } else
        this.addItem(MenuItem.createLabel(jsonBase[index].text, jsonBase[index].icon));
    }
  }

  this.open = function() {
    _open = true;
    menuObject.style.display = "block";
    /*return;
    if (menuItems.length == 0)
    {
      menuObject.style.display = "block";
      menuObject.style.opacity = 1;
      menuObject.style.filter = "alpha(opacity=100)";
    } else
      fadeIn("menu-" + handle);*/
  };
  this.close = function(close_parents) {
    if (typeof close_parents == "undefined")
      var close_parents = false;
    if (close_parents)
    {
      var topMenu = this;
      while (topMenu.parentMenu)
        topMenu = topMenu.parentMenu;
      topMenu.close();
      return;
    }
    
    for (var index = 0; index < childMenus.length; index++)
      childMenus[index].close();
    _open = false;
    menuObject.style.display = "none";
    /*return;
    if (menuItems.length == 0)
    {
      menuObject.style.display = "none";
      menuObject.style.opacity = 0;
      menuObject.style.filter = "alpha(opacity=0)";
    } else
      fadeOut("menu-" + handle);*/
  };
  var childMenus = new Array();
  this.registerChildMenu = function(child_menu) {
    child_menu.parentMenu = this;
    childMenus.push(child_menu);
  };
}

function MenuItem(itemLabel, itemIcon, gID, isDefault)
{
  if (!isDefault)
    var isDefault = false;
  var container = document.createElement("div");
  container.className = "entry" + (isDefault ? " default" : "");
  container.menuItem = this;
  this.getContainer = function() { return container; };
  this.type = null;
  var parentMenu = null;
  this.onparentmenuset = null;
  this.setParentMenu = function(menu)
  {
    parentMenu = menu;
    if (this.onparentmenuset)
      this.onparentmenuset(menu);
  };
  var onClicked = null;
  var icon = document.createElement("img");
  icon.className = "icon";
  if (itemIcon)
    icon.src = web_path + "/" + itemIcon;
  else
    icon.style.display = "none";
  container.appendChild(icon);
  var label = document.createElement("div");
  label.className = "label";
  if (itemLabel != undefined)
    label.innerHTML = itemLabel;
  container.appendChild(label);
  container.onclick = function(e)
  {
    if (!onClicked)
      return;
    if (!e)
      e = window.event;
    parentMenu.close(true);
    onClicked(gID);
  };
  this.setElementClass = function(class_name)
  {
    container.className += " " + class_name;
  };
  this.setInteraction = function(interaction_function)
  {
    if (!interaction_function)
    {
      onClicked = null;
      container.className = "entry";
    } else
    {
      onClicked = interaction_function;
      container.className = "entry hasInteractions";
    }
  };
  this.getText = function() { return label.innerHTML; };
  this.setText = function(new_value) { label.innerHTML = new_value; };
}

MenuItem.createLabel = function(labelText)
{
  var label = new MenuItem(labelText);
  return label;
};
MenuItem.createSeparator = function()
{
  var separator = new MenuItem();
  separator.setElementClass("separator");
  return separator;
};
MenuItem.createButton = function(text, icon, gID, isDefault)
{
  var button = new MenuItem(text, icon, gID, isDefault);
  return button;
}
MenuItem.createSubmenu = function(text, icon, submenu)
{
  var submenuItem = new MenuItem(text, icon, submenu);
  submenuItem.type = "submenu";
  submenuItem.submenu = submenu;
  submenuItem.onparentmenuset = function(menu) { menu.registerChildMenu(submenu); };
  submenuItem.setElementClass("submenu");
  submenuItem.getContainer().onmouseover = function(e)
  {
    if (!e)
      var e = window.event;
    var parentX = -1;
    var parentY = 0;
    var currentItem = submenuItem.getContainer();
    while (currentItem)
    {
      parentX += currentItem.offsetLeft;
      parentY += currentItem.offsetTop;
      currentItem = currentItem.offsetParent;
    }
    var locationX = 0;
    var locationY = 0;
    if (parentX + submenu.getWidth() >= window.screen.width)
      locationX = parentX - submenu.getWidth();
    else
      locationX = parentX + submenuItem.getContainer().offsetWidth;
    if (parentY + submenu.getHeight() >= window.screen.height)
      locationY = parentY - submenu.getHeight();
    else
      locationY = parentY;
    submenu.setTop(locationY.toString() + "px");
    submenu.setLeft(locationX.toString() + "px");
    if (!submenu.isOpen())
      submenu.open();
  };
  submenuItem.getContainer().onmouseout = function(e)
  {
    if (!submenu.isOpen())
      return;
    if (!e)
      var e = window.event;
    
    var currentItem = (e.toElement || e.relatedTarget);
    while (currentItem)
    {
      if (currentItem.menu)
      {
        if (currentItem.menu == submenu)
        {
          return;
        }
      }
      currentItem = currentItem.parentNode;
    }
    submenu.close();
  };
  /*submenuItem.getContainer().onmouseout = function(e)
  {
    if (!e)
      var e = window.event;
    trail = new Array();
    var destinationElement = e.toElement || e.relatedTarget;
    while (destinationElement)
    {
      trail.push(destinationElement);
      if (destinationElement.menu)
      {
        var destMenu = destinationElement.menu;
        while (destMenu)
        {
          trail.push(destMenu);
          if (destMenu == submenu)
            return;
          destMenu = destMenu.parentMenu;
        }
      }
      destinationElement = destinationElement.parentNode;
    }
    submenu.close();
  };*/
  return submenuItem;
};
var trail = new Array();
