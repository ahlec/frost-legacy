function Files() { };
Files.associations = new Array();
Files.loadAssociations = function()
{
  executeAJAX("scripts/get-file-associations.php", function parse(results)
  {
    associations = eval("(" + results + ")");
    for (var index = 0; index < associations.associations.length; index++)
    {
      Files.associations.push([associations.associations[index].mime,
	associations.associations[index].icon,
	associations.associations[index].program]);
    }
  });
}
Files.getInformation = function(file_mime)
{
  for (var index = 0; index < Files.associations.length; index++)
  {
    if (Files.associations[index][0] == file_mime)
      return Files.associations[index];
  }
  return new Array(file_mime, "unknown.png", "");
};
