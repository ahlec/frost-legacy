function Programs() { };
Programs.programs = new Array();
Programs.load = function()
{
  executeAJAX("scripts/get-programs.php", function parse(results)
  {
    programs = eval("(" + results + ")");
    for (var index = 0; index < programs.length; index++)
    {
      Programs.programs.push(programs[index]);
      var programJavascript = document.createElement("script");
      programJavascript.base = programs[index];
      programJavascript.onload = function()
      {
        if (!this.base)
          return;
        if (this.base.isApplication == 'true')
        {
          var programButton = MenuItem.createButton(this.base.title, this.base.icon);
	      programButton.setInteraction(eval(this.base.handle + ".open"));
	      Taskbar.programsMenu.addItem(programButton);
        }
      };
      programJavascript.src = web_path + "/programs/" +
	programs[index].file;
      document.getElementsByTagName("head")[0].appendChild(programJavascript);
    }
  });
}
Programs.getByHandle = function(handle)
{
  for (var index = 0; index < Programs.programs.length; index++)
    if (Programs.programs[index].handle == handle)
      return Programs.programs[index];
  return null;
}