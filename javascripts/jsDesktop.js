function Desktop() { };

Desktop.contents = null;
Desktop.getContextInfo = function() { return {gid:1}; };
Desktop.getDropInfo = function() { return {takes: true, gid: 1}; };

Desktop.load = function()
{
  executeAJAX("scripts/get-location.php?identity=1", function load(desktop)
  {
    desktop = eval("(" + desktop + ")");
    Desktop.contents = new LocationContents(1, desktop.contents);
    Desktop.contents.setGId(1);
    Desktop.contents.applyTo(document.getElementById("background-image"));
    document.getElementById("background-image").contextHandles = desktop.contextHandles;
    document.getElementById("background-image").base = Desktop;
  });
};
