function Login() { }
Login.screenOpen = false;
Login.background = null;
Login.panel = null;
Login.usernameBox = null;
Login.passwordBox = null;
Login.button = null;

Login.openScreen = function()
{
  if (Login.screenOpen)
    return;
  Login.screenOpen = true;
  
  Login.background = document.createElement("div");
  Login.background.className = "loginBackground";
  document.body.appendChild(Login.background);
  
  Login.panel = document.createElement("div");
  Login.panel.className = "loginPanel";
  document.body.appendChild(Login.panel);
  
  Login.usernameBox = document.createElement("input");
  Login.usernameBox.type = "text";
  Login.usernameBox.value = "username";
  Login.usernameBox.className = "loginTextbox username empty";
  Login.usernameBox.onkeyup = Login.checkFormComplete;
  Login.usernameBox.onfocus = function()
  {
    if (Login.usernameBox.className == "loginTextbox username empty")
	{
	  Login.usernameBox.value = "";
	  Login.usernameBox.className = "loginTextbox username";
	}
  };
  Login.usernameBox.onblur = function()
  {
    if (Login.usernameBox.value.length == 0)
	{
	  Login.usernameBox.value = "username";
	  Login.usernameBox.className = "loginTextbox username empty";
	}
  };
  Login.panel.appendChild(Login.usernameBox);
  
  Login.passwordBox = document.createElement("input");
  Login.passwordBox.type = "text";
  Login.passwordBox.value = "password";
  Login.passwordBox.onkeyup = Login.checkFormComplete;
  Login.passwordBox.className = "loginTextbox password empty";
  Login.passwordBox.onfocus = function()
  {
    if (Login.passwordBox.className == "loginTextbox password empty")
	{
	  Login.passwordBox.value = "";
	  Login.passwordBox.type = "password";
	  Login.passwordBox.className = "loginTextbox password";
	}
  };
  Login.passwordBox.onblur = function()
  {
    if (Login.passwordBox.value.length == 0)
	{
	  Login.passwordBox.value = "password";
	  Login.passwordBox.type = "text";
	  Login.passwordBox.className = "loginTextbox password empty";
	}
  };
  Login.panel.appendChild(Login.passwordBox);
  
  Login.button = document.createElement("input");
  Login.button.type = "image";
  Login.button.src = web_path + "/images/interface/loginButton.png";
  Login.button.onclick = Login.processLogin;
  Login.button.className = "loginButton";
  Login.button.disabled = true;
  Login.button.style.display = "none";
  Login.panel.appendChild(Login.button);
};

Login.checkFormComplete = function(e)
	{
	  if (!e)
	    var e = window.event;
	  Login.button.disabled = 
		(Login.usernameBox.value.length == 0 ||
		 Login.usernameBox.className == "loginTextbox username empty" ||
		Login.usernameBox.value.match(/[^0-9a-zA-Z ]/) || 
		Login.passwordBox.className == "loginTextbox password empty" ||
		Login.passwordBox.value.length == 0);
	  Login.button.style.display = (Login.button.disabled ? "none" : "block");
	  if ((e.which || e.keyCode) == 13 && !Login.button.disabled)
		Login.button.click();
	};
Login.processLogin = function()
{
  if (Login.button.disabled)
    return;
  Login.usernameBox.disabled = true;
  Login.passwordBox.disabled = true;
  Login.button.disabled = true;
  var loginCall = "scripts/login.php?username=" + Login.usernameBox.value +
	"&password=" + hex_md5(Login.passwordBox.value);
  executeAJAX(loginCall, function(results)
  {
    if (results == 'success')
    {
	  if (location.hash.length > 0)
	  {
	    window.location = decodeURIComponent(location.hash.substr(1));
	    return;
	  }
      Login.usernameBox = null;
      Login.passwordBox = null;
      Login.button = null;
      document.body.removeChild(Login.panel);
      Login.panel = null;
      document.body.removeChild(Login.background);
      Login.background = null;
      Login.screenOpen = false;
      setupDesktop();
      return;
    }
    alert(results.replace('[Error] ', ''));
    Login.usernameBox.disabled = false;
    Login.passwordBox.disabled = false;
    Login.passwordBox.value = '';
    Login.passwordBox.focus();
    Login.passwordBox.select();
  });
};