//Taken from http://www.switchonthecode.com/tutorials/javascript-tutorial-simple-fade-animation
var TimeToFade = 175.0;

function fadeIn(eid)
{
  var element = document.getElementById(eid);
  if (!element)
    return;
  element.FadeState = -2;
  element.style.opacity = 0;
  element.style.filter = "alpha(opacity=0)";
  element.style.display = "block";
  fade(eid);
}
function fadeOut(eid)
{
  var element = document.getElementById(eid);
  if (!element)
    return;
  element.FadeState = 2;
  element.style.opacity = 1;
  element.style.filter = "alpha(opacity=100)";
  element.style.display = "block";
  fade(eid);
}
animateFade = function() //function animateFade()//lastTick, eid)
{  
  var element = this;
  var curTick = new Date().getTime();
  var lastTick = element.lastTick;
  var elapsedTicks = curTick - lastTick;
//  var element = document.getElementById(eid);
 
  if(element.FadeTimeLeft <= elapsedTicks)
  {
    element.style.opacity = element.FadeState == 1 ? '1' : '0';
    element.style.filter = 'alpha(opacity = ' 
        + (element.FadeState == 1 ? '100' : '0') + ')';
    //Hide after fading out
    if(element.FadeState == -1) { element.style.display = 'none'; }
    element.FadeState = element.FadeState == 1 ? 2 : -2;
    return;
  }
 
  element.FadeTimeLeft -= elapsedTicks;
  var newOpVal = element.FadeTimeLeft/TimeToFade;
  if(element.FadeState == 1)
    newOpVal = 1 - newOpVal;

  element.style.opacity = newOpVal;
  element.style.filter = 'alpha(opacity = ' + (newOpVal*100) + ')';
  
  element.lastTick = curTick;
  setTimeout(animateFade.bind(element), 33);
//  setTimeout("animateFade(" + curTick + ",'" + eid + "')", 33);
}

function fade(eid)
{
  var element = document.getElementById(eid);
  if(element == null)
    return;

  if(element.FadeState == null)
  {
    if(element.style.opacity == null 
        || element.style.opacity == '' 
        || element.style.opacity == '1')
    {
      element.FadeState = 2;
    }
    else
    {
      element.FadeState = -2;
    }
  }
  
  if(element.FadeState == 1 || element.FadeState == -1)
  {
    element.FadeState = element.FadeState == 1 ? -1 : 1;
    element.FadeTimeLeft = TimeToFade - element.FadeTimeLeft;
  }
  else
  {
    element.lastTick = new Date().getTime();
    element.FadeState = element.FadeState == 2 ? -1 : 1;
    //Show before fading in
    if(element.FadeState == 1) { element.style.display = 'block'; }
    element.FadeTimeLeft = TimeToFade;

    var timeoutFunction = animateFade.bind(element);
    setTimeout(timeoutFunction, 33);
//    setTimeout("animateFade(" + new Date().getTime() + ",'" + eid + "')", 33);
  }  
}