/* Browser window processing */
window.currentHeight = document.documentElement.clientHeight;
window.onresize = function()
  {
    if (window.currentHeight == document.documentElement.clientHeight)
      return;
    window.currentHeight = document.documentElement.clientHeight;
    var openFrames = Frame.getAll();
    for (var index = 0; index < openFrames.length; index++)
      openFrames[index].processNewWindowSize();
  };

/* Setup Desktop */
function setupDesktop()
{
  executeAJAX('scripts/load-current-user.php', function process(results)
  {
    if (results == "")
    {
      Login.openScreen();
      return;
    }
    if (Frame.getByHandle("login-panel"))
      Frame.getByHandle("login-panel").close();
    var userInformation = eval("(" + results + ")");
    ContextMenu.initialize();
    Profile.load(userInformation);
    Files.loadAssociations();
    Programs.load();
    Taskbar.create();
    Theme.apply(userInformation.theme);
    Desktop.load();
  });
}

/* Logout */
function logout()
{
  executeAJAX('scripts/logout.php', function process(results)
  {
    window.location = web_path;
  });
}
