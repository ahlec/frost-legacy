function input(inputString)
{
  if (typeof inputString == "undefined")
    var inputString = "";
  if (!document.activeElement)
    return;
  var caretPosition = 0;
  if (document.selection)
  {
    var selection = document.selection.createRange();
    selection.moveStart('character', -document.activeElement.value.length);
    caretPosition = selection.text.length;
  } else
    caretPosition = document.activeElement.selectionStart;
}

if (!Element.prototype.rightClick)
  Element.prototype.rightClick = function()
  {
    if (this.ownerDocument.createEventObject)
    {
      var contextEvent = this.ownerDocument.createEventObject(window.event);
      contextEvent.button = 2;
      this.fireEvent('oncontextmenu', contextEvent);
    } else
    {
      var contextEvent = this.ownerDocument.createEvent("MouseEvents");
      contextEvent.initMouseEvent('contextmenu', true, true, this.ownerDocument.defaultView,
	1, 0, 0, 0, 0, false, false, false, false, 2, null);
      this.dispatchEvent(contextEvent);
    }
  };
  
function getElementsByLocation(x, y)
{
  var elements = new Array();
  for (var index = 0; index < document.all.length; index++)
  {
    if (!document.all[index].offsetParent)
      continue;
    var elementX = elementY = 0;
    var currentElement = document.all[index];
    do
    {
      elementX += currentElement.offsetLeft;
      elementY += currentElement.offsetTop;
    } while (currentElement = currentElement.offsetParent);
    if (elementX <= x && elementY <= y && elementX + document.all[index].offsetWidth >= x &&
    	elementY + document.all[index].offsetHeight >= y)
    	elements.push(document.all[index]);
  }
  return elements;
}