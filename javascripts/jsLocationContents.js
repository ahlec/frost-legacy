function LocationContents(locationGId, jsonBase)
{
  var parentElement = null;
  var itemIcons = new Array();
  var locationContents = this;
  var registeredGId = locationGId;
  LocationContents.allGroups.push(this);
  
  var _itemsortName = function(a, b)
  {
    if (a.getName() == b.getName())
      return 0;
    return (a.getName() < b.getName() ? -1 : 1);
  };
  var _itemsortGId = function(a, b)
  {
    return a.getGId() - b.getGId();
  }
  
  var selectedSort = _itemsortGId;

  this.addItem = function(json)
  {
    var newIconItem = new IconItem(json.icon, json.name, json.contextHandles, json.defaultProgram, json.type, json.gID, locationGId, locationContents);
    itemIcons.push(newIconItem);
    if (parentElement)
      parentElement.appendChild(newIconItem.container);
  };
  
  for (var index = 0; index < jsonBase.length; index++)
    this.addItem(jsonBase[index]);
  itemIcons.sort(selectedSort);
  
  this.setGId = function(gid) { registeredGId = gid; };
  this.getGId = function() { return registeredGId; };
  this.applyTo = function(element)
  {
    for (var index = 0; index < itemIcons.length; index++)
      element.appendChild(itemIcons[index].container);
    parentElement = element;
  };
  this.getParent = function() { return parentElement; };
  this.destroy = function()
  {
    for (var index = 0; index < itemIcons.length; index++)
      itemIcons[index].destroy();
    if (LocationContents.allGroups.indexOf(locationContents) > -1)
      LocationContents.allGroups.splice(LocationContents.allGroups.indexOf(locationContents), 1);
  };
  this.onblur = null;
  this.onselection = null;
  
}
LocationContents.allGroups = new Array();
LocationContents.getByGId = function(gid)
{
  var results = new Array();
  for (var index = 0; index < LocationContents.allGroups.length; index++)
    if (LocationContents.allGroups[index].getGId() == gid)
      results.push(LocationContents.allGroups[index]);
  return results;
}
