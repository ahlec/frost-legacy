# frOSt Legacy#

Legacy version of the frOSt filesystem, before the 2014 complete redesign. The more visually and functionally complete version, it is the outdated version and uses poorer coding practises, and is not being 
maintained any longer.

This is version 2 ("Filesys v2 - Tome"), the longest-running version of frOSt, having been started roughly in 2009 and ending in 2014 with the bottom-up redesign of frOSt. Please keep in mind the age when looking at the code and judging me (wow, I've progressed so far since this time).

### Hosting ###
I'm working on reuploading this at the moment and hope to have a rehosted version of frOSt legacy by the end of the next day or two. Information will be posted here once I have.